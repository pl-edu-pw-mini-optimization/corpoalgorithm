package javabbob;

import java.io.IOException;
import java.util.Random;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.DoublePoint;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

/**
 * Wrapper class running an entire BBOB experiment. It illustrates the
 * benchmarking of MY_OPTIMIZER on the noise-free testbed or the noisy testbed
 * (change the ifun loop in this case as given below). This class reimplements
 * the content of exampleexperiment.c from the original C version of the BBOB
 * code.
 */
public class ExampleExperiment {

    private static int psoN = 10;
    private static boolean searchForPeak = true;
    private static int n = 40;
    private static double c1 = 1.4;
    private static double c2 = 1.4;
    private static double omega = 0.64;
    private static boolean useVNS = true;
    private static boolean randomChangeOfParticles = true;

    /**
     * Example optimiser. In the following, the pure random search optimization
     * method is implemented as an example. Please include/insert any code as
     * suitable.<p>
     * This optimiser takes as argument an instance of JNIfgeneric which have
     * all the information on the problem to solve. Only the methods
     * getFtarget() and evaluate(x) of the class JNIfgeneric are used.<p>
     * This method also takes as argument an instance of Random since one might
     * want to set the seed of the random search.<p>
     * The optimiser generates random vectors evaluated on fgeneric until the
     * number of function evaluations is greater than maxfunevals or a function
     * value smaller than the target given by fgeneric.getFtarget() is attained.
     * The parameter maxfunevals to avoid problem when comparing it to
     * 1000000000*dim where dim is the dimension of the problem.
     *
     * @param fgeneric an instance JNIfgeneric object
     * @param dim an integer giving the dimension of the problem
     * @param maxfunevals the maximum number of function evaluations
     * @param rand an instance of Random
     */
    public static void MY_OPTIMIZER(JNIfgeneric fgeneric, int dim, double maxfunevals, Random rand) {

        double[][] x = new double[n][dim];
        double[][] best = new double[n][dim];
        double[][] bestNeighbours = new double[n][dim];
        double[][] v = new double[n][dim];
        ArrayList<double[][]> previousData = new ArrayList<double[][]>();
        ArrayList<double[]> previousValues = new ArrayList<double[]>();

        /* Obtain the target function value, which only use is termination */
        double ftarget = fgeneric.getFtarget();
        double[] f = new double[n];
        double[] bestValues = new double[n];
        double[] bestValuesNeighbours = new double[n];
        double bestValue = Double.MAX_VALUE;
        double[] bestOfTheBest = new double[dim];

        initializePosition(n, dim, x, rand, bestValues, bestValuesNeighbours);
        initializeVelocity(n, rand, dim, v, x);
        computeFunction(n, f, fgeneric, x);
        bestValue = updateBest(n, f, bestValues, dim, best, x, bestValue, bestValuesNeighbours, bestNeighbours, bestOfTheBest);

        if (maxfunevals > 1e9 * dim) {
            maxfunevals = 1e9 * dim;
        }

        for (double iter = 0.; iter < maxfunevals; iter += n) {
            updateLocation(n, dim, x, v);
            computeFunction(n, f, fgeneric, x);
            bestValue = updateBest(n, f, bestValues, dim, best, x, bestValue, bestValuesNeighbours, bestNeighbours, bestOfTheBest);

            updateVelocity(n, dim, v, omega, c1, rand, best, x, f, c2, bestNeighbours, previousData, previousValues);
            if (bestValue < ftarget) {
                break;
            }

            if (evalsSinceImprovement > dim * 100) {
                break;
            }

        }
        if (useVNS) {
            performVNS(dim, rand, 0.5, bestOfTheBest, fgeneric, bestValue);
        }

    }

    private static double performVNS(int dim, Random rand, double step, double[] bestOfTheBest, JNIfgeneric fgeneric, double bestValue) {
        double[] direction = new double[dim];
        double[] newDirection = new double[dim];
        double[] testPoint = new double[dim];
        double testValue;
        int missedTries = 2 * dim;
        for (int iterations = 0; iterations < dim * 1000; ++iterations) {
            for (int i = 0; i < dim; ++i) {
                newDirection[i] = rand.nextGaussian() * step + direction[i] / 2;
                testPoint[i] = bestOfTheBest[i] + newDirection[i];
            }
            testValue = fgeneric.evaluate(testPoint);
            if (testValue <= bestValue) {
                for (int i = 0; i < dim; ++i) {
                    bestOfTheBest[i] = testPoint[i];
                    direction[i] = newDirection[i];
                    missedTries = 2 * dim;
                }
                bestValue = testValue;
            } else {
                for (int i = 0; i < dim; ++i) {
                    testPoint[i] = bestOfTheBest[i] - newDirection[i];
                }
                testValue = fgeneric.evaluate(testPoint);
                ++iterations;
                if (testValue <= bestValue) {
                    for (int i = 0; i < dim; ++i) {
                        bestOfTheBest[i] = testPoint[i];
                        direction[i] = -newDirection[i];
                        missedTries = 2 * dim;
                    }
                    bestValue = testValue;
                } else {
                    if (--missedTries <= 0) {
                        missedTries = 2 * dim;
                        step /= 1.2;
                    };
                }
            }
        }
        return bestValue;
    }

    private static void updateVelocity(int n, int dim, double[][] v, double omega, double c1, Random rand, double[][] best, double[][] x, double[] f, double c2, double[][] bestNeighbours, ArrayList<double[][]> previousData, ArrayList<double[]> previousValues) {
        storeSamples(previousData, n, dim, previousValues, x, f);

        double[][] xdata = new double[previousData.size() * n][2 * dim + 1];
        double[] fdata = new double[previousValues.size() * n];
        extractSamples(previousData, n, dim, xdata, previousValues, fdata);

        int peakParticle = -1;
        double[] peak = new double[dim];
        if (searchForPeak && previousData.size() * n > 2 * dim + 1) {
            peakParticle = findPeak(fdata, xdata, peakParticle, rand, n, dim, peak, best);
        }

        HashSet<Integer> psoParticles = new LinkedHashSet<>();
        if (randomChangeOfParticles) {
            while (psoParticles.size() < psoN) {
                psoParticles.add(rand.nextInt(n));
            }
        } else {
            for (int particleIdx = 0; particleIdx < psoN; particleIdx++) {
                psoParticles.add(particleIdx);
            }
        }

        for (int j = 0; j < n; ++j) {
            int n2 = n, n3 = n, n4 = n;
            while (n2 == n) {
                n2 = rand.nextInt(n);
            }
            while (n3 == n || n3 == n2) {
                n3 = rand.nextInt(n);
            }
            while (n4 == n || n4 == n3 || n4 == n2) {
                n4 = rand.nextInt(n);
            }
            for (int i = 0; i < dim; i++) {
                if (j == peakParticle) {
                    v[j][i] = -x[j][i] + peak[i];
                } else if (psoParticles.contains(j)) {
                    v[j][i] = omega * v[j][i]
                            + c1 * rand.nextDouble() * (best[j][i] - x[j][i])
                            + c2 * rand.nextDouble() * (bestNeighbours[j][i] - x[j][i]);
                } else {
                    v[j][i] = (rand.nextBoolean() ? (best[n4][i] + 0.5 * (best[n2][i] - best[n3][i])) : (best[j][i]))
                            - x[j][i];

                }
            }
        }

    }

    public static class Sample implements Clusterable {

        public Sample(double val, double[] x, int dim) {
            this.val = val;
            this.x = x;
            this.location = new double[dim];
            for (int i = 0; i < dim; ++i) {
                this.location[i] = this.x[i];
            }
        }

        double val;
        double[] x;
        double[] location;

        @Override
        public double[] getPoint() {
            return location;
        }

        public double getValue() {
            return val;
        }

        public double[] getSample() {
            return x;
        }

    }

    private static int findPeak(double[] fdata, double[][] xdata, int peakParticle, Random rand, int n, int dim, double[] peak, double[][] best) throws MathIllegalArgumentException {
        //TODO: find most promising peak = better clusters + their comparison
        CentroidCluster<Sample> largest = getClusterFromSamples(xdata, fdata, dim, rand);
        fdata = new double[largest.getPoints().size()];
        xdata = new double[largest.getPoints().size()][2 * dim + 1];
        extractSamplesFromCluster(largest, xdata, fdata);
        OLSMultipleLinearRegression lm = new OLSMultipleLinearRegression();
        lm.setNoIntercept(true);
        lm.newSampleData(fdata, xdata);
        double[] parameters = lm.estimateRegressionParameters();
        peakParticle = rand.nextInt(n);
        for (int i = 0; i < dim; ++i) {
            if (parameters[i] > 1e-8) {
                peak[i] = -parameters[i + dim] / 2.0 / parameters[i];
                if (peak[i] > 5.0) {
                    peak[i] = 5.0;
                }
                if (peak[i] < -5.0) {
                    peak[i] = -5.0;
                }
            } else {
                peak[i] = best[peakParticle][i];
            }
        }
        return peakParticle;
    }

    private static void extractSamplesFromCluster(CentroidCluster<Sample> largest, double[][] xdata, double[] fdata) {
        int k = 0;
        for (Sample sample : largest.getPoints()) {
            xdata[k] = sample.getSample();
            fdata[k] = sample.getValue();
            ++k;
        }
    }

    private static CentroidCluster<Sample> getClusterFromSamples(double[][] xdata, double[] fdata, int dim, Random rand) throws ConvergenceException, MathIllegalArgumentException {
        Collection<Sample> points = new ArrayList<Sample>();
        for (int j = 0; j < xdata.length; ++j) {
            points.add(new Sample(fdata[j], xdata[j], dim));
        }
        KMeansPlusPlusClusterer<Sample> clusterer = new KMeansPlusPlusClusterer<Sample>(rand.nextInt(
                (int) Math.max(1,
                        Math.ceil(xdata.length / (2 * dim + 1) / 5))));
        Iterator<CentroidCluster<Sample>> iterator = clusterer.cluster(points).iterator();
        CentroidCluster<Sample> largest = null;
        while (iterator.hasNext()) {
            CentroidCluster<Sample> candidate = iterator.next();
            if (largest == null || largest.getPoints().size() < candidate.getPoints().size()) {
                largest = candidate;
            }
            if ((2 * dim + 1) * 5 < largest.getPoints().size()) {
                break;
            }
        }
        return largest;
    }

    private static void extractSamples(ArrayList<double[][]> previousData, int n, int dim, double[][] xdata, ArrayList<double[]> previousValues, double[] fdata) {
        int k = 0;
        for (double[][] smallData : previousData) {

            for (int j = 0; j < n; ++j) {
                for (int i = 0; i < 2 * dim + 1; ++i) {
                    xdata[k + j][i] = smallData[j][i];
                }
            }
            k += n;
        }

        k = 0;
        for (double[] smallData : previousValues) {
            for (int j = 0; j < n; ++j) {
                fdata[k + j] = smallData[j];
            }
            k += n;
        }
    }

    private static void storeSamples(ArrayList<double[][]> previousData, int n, int dim, ArrayList<double[]> previousValues, double[][] x, double[] f) {
        if (previousData.size() * n >= 10 * dim) {
            previousData.remove(0);
            previousValues.remove(0);
        }
        double[][] xdata = new double[n][2 * dim + 1];
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < dim; ++i) {
                xdata[j][2 * dim] = 1.0;
                xdata[j][i] = x[j][i] * x[j][i];
                xdata[j][i + dim] = x[j][i];
            }
        }
        previousValues.add(f);
        previousData.add(xdata);
    }

    private static void updateLocation(int n, int dim, double[][] x, double[][] v) {
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < dim; i++) {
                x[j][i] = x[j][i] + v[j][i];
            }
        }
    }

    private static double updateBest(int n, double[] f, double[] bestValues, int dim, double[][] best, double[][] x, double bestValue, double[] bestValuesNeighbours, double[][] bestNeighbours, double[] bestOfTheBest) {
        for (int j = 0; j < n; ++j) {
            if (f[j] <= bestValues[j]) {
                for (int i = 0; i < dim; ++i) {
                    bestNeighbours[j][i] = best[j][i] = x[j][i];
                }
                bestValues[j] = f[j];
                ++evalsSinceImprovement;
                if (bestValues[j] < bestValue) {
                    evalsSinceImprovement = 0;
                    bestValue = bestValues[j];
                    for (int i = 0; i < dim; ++i) {
                        bestOfTheBest[i] = best[j][i] = x[j][i];
                    }
                }
            }
            if (j > 0) {
                int j2 = j - 1;
                chooseUpdateNeighbourBest(bestValues, j, j2, bestValuesNeighbours, dim, bestNeighbours);
                if (j == n - 1) {
                    chooseUpdateNeighbourBest(bestValues, j, 0, bestValuesNeighbours, dim, bestNeighbours);
                }

            }

        }
        return bestValue;
    }

    private static void computeFunction(int n, double[] f, JNIfgeneric fgeneric, double[][] x) {
        /* evaluate X on the objective function */
        for (int j = 0; j < n; ++j) {
            f[j] = fgeneric.evaluate(x[j]);
        }
    }

    private static void chooseUpdateNeighbourBest(double[] bestValues, int j, int j2, double[] bestValuesNeighbours, int dim, double[][] bestNeighbours) {
        if (bestValues[j] > bestValues[j2]) {
            updateNeighbourBest(bestValuesNeighbours, j, bestValues, j2, dim, bestNeighbours);
        } else {
            updateNeighbourBest(bestValuesNeighbours, j2, bestValues, j, dim, bestNeighbours);
        }
    }

    private static void updateNeighbourBest(double[] bestValuesNeighbours, int j, double[] bestValues, int j2, int dim, double[][] bestNeighbours) {
        bestValuesNeighbours[j] = bestValues[j2];
        for (int i = 0; i < dim; ++i) {
            bestNeighbours[j][i] = bestNeighbours[j2][i];
        }
    }

    static int evalsSinceImprovement = 0;

    private static void initializeVelocity(int n, Random rand, int dim, double[][] v, double[][] x) {
        for (int j = 0; j < n; ++j) {
            int p1 = rand.nextInt(n);
            int p2 = rand.nextInt(n);
            for (int i = 0; i < dim; i++) {
                v[j][i] = (x[p1][i] - x[p2][i]) / 2;
            }
        }
    }

    private static void initializePosition(int n, int dim, double[][] x, Random rand, double[] bestValues, double[] bestNeighbours) {
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < dim; i++) {
                x[j][i] = 10. * rand.nextDouble() - 5.;
            }
            bestValues[j] = Double.MAX_VALUE;
            bestNeighbours[j] = Double.MAX_VALUE;
        }
    }

    /**
     * Main method for running the whole BBOB experiment. Executing this method
     * runs the experiment. The first command-line input argument is
     * interpreted: if given, it denotes the data directory to write in (in
     * which case it overrides the one assigned in the preamble of the method).
     */
    public static void main(String[] args) throws IOException {

        /* run variables for the function/dimension/instances loops */
        //final int dim[] = {2, 3, 5, 10, 20};
        final int dim[] = {5, 20};
        final int instances[] = {1, 2, 3, 4, 5, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50};
        int idx_dim, ifun, idx_instances, independent_restarts;
        double maxfunevals;
        double evalsPerDim = 1e5;
        String outputPath;

        JNIfgeneric fgeneric = new JNIfgeneric();
        /* The line above loads the library cjavabbob at the core of
         * JNIfgeneric. It will throw runtime errors if the library is not
         * found.
         */

        /**
         * ************************************************
         * BBOB Mandatory initialization *
         * ***********************************************
         */
        JNIfgeneric.Params params = new JNIfgeneric.Params();
        /* Modify the following parameters, choosing a different setting
         * for each new experiment */
        if (args.length > 0) {
            evalsPerDim = Double.parseDouble(args[0]); // Warning: might override the assignment above.
        }

        if (args.length > 4) {
            ExampleExperiment.n = Integer.parseInt(args[1]);
            ExampleExperiment.psoN = Integer.parseInt(args[2]);
            ExampleExperiment.searchForPeak = Boolean.parseBoolean(args[3]);
            ExampleExperiment.useVNS = Boolean.parseBoolean(args[4]);
        }
        params.algName = "PSO" + psoN + "_DE" + (n - psoN) + (searchForPeak ? "_LM" : "") + (useVNS ? "_VNS" : "") + (randomChangeOfParticles ? "_RAND" : "");
        params.comments = "PSO hybrydized with DE particles and LM particle and VNS at the end";
//        if (args.length > 1) {
//            params.algName = args[1]; // Warning: might override the assignment above.
//        }

        outputPath = params.algName + evalsPerDim;

        /* Creates the folders for storing the experimental data. */
        if (JNIfgeneric.makeBBOBdirs(outputPath, true)) {
            System.out.println("BBOB data directories at " + outputPath
                    + " created.");
        } else {
            System.out.println("Error! BBOB data directories at " + outputPath
                    + " was NOT created, stopping.");
            return;
        };


        /* External initialization of MY_OPTIMIZER */
        long seed = System.currentTimeMillis();
        Random rand = new Random(seed);
        System.out.println("MY_OPTIMIZER seed: " + seed);

        /* record starting time (also useful as random number generation seed) */
        long t0 = System.currentTimeMillis();

        /* To make the noise deterministic, uncomment the following block. */
        /* int noiseseed = 30; // or (int)t0
         * fgeneric.setNoiseSeed(noiseseed);
         * System.out.println("seed for the noise set to: "+noiseseed); */

        /* now the main loop */
        for (idx_dim = 0; idx_dim < dim.length; idx_dim++) {
            /* Function indices are from 1 to 24 (noiseless) or from 101 to 130 (noisy) */
            /* for (ifun = 101; ifun <= 130; ifun++) { // Noisy testbed */
            for (ifun = 1; ifun <= 24; ifun++) { //Noiseless testbed
                for (idx_instances = 0; idx_instances < instances.length; idx_instances++) {

                    /* Initialize the objective function in fgeneric. */
                    fgeneric.initBBOB(ifun, instances[idx_instances],
                            dim[idx_dim], outputPath, params);
                    /* Call to the optimizer with fgeneric as input */
                    maxfunevals = evalsPerDim * dim[idx_dim]; /* PUT APPROPRIATE MAX. FEVALS */
                    /* 5. * dim is fine to just check everything */

                    independent_restarts = -1;
                    while (fgeneric.getEvaluations() < maxfunevals) {
                        independent_restarts++;
                        MY_OPTIMIZER(fgeneric, dim[idx_dim],
                                maxfunevals - fgeneric.getEvaluations(), rand);
                        if (fgeneric.getBest() < fgeneric.getFtarget()) {
                            break;
                        }
                    }

                    System.out.printf("  f%d in %d-D, instance %d: FEs=%.0f with %d restarts,", ifun, dim[idx_dim],
                            instances[idx_instances], fgeneric.getEvaluations(), independent_restarts);
                    System.out.printf(" fbest-ftarget=%.4e, elapsed time [h]: %.2f\n", fgeneric.getBest() - fgeneric.getFtarget(),
                            (double) (System.currentTimeMillis() - t0) / 3600000.);

                    /* call the BBOB closing function to wrap things up neatly */
                    fgeneric.exitBBOB();
                }

                System.out.println("\ndate and time: " + (new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")).format(
                        (Calendar.getInstance()).getTime()));

            }
            System.out.println("---- dimension " + dim[idx_dim] + "-D done ----");
        }
        //system(paste0("python python\\bbob_pproc\\rungeneric.py ",name));
        //system(paste0("pdflatex -jobname ",name," templateCECBBOBarticle.tex >log.log"))
        //system(paste0("pdflatex -jobname ",name," templateCECBBOBarticle.tex >log.log"))

        Runtime.getRuntime().exec("python ../BBOB/python/bbob_pproc/rungeneric.py " + outputPath);
    }
}

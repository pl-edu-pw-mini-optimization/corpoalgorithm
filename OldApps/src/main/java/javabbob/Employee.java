/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob;

import java.util.Arrays;
import java.util.Set;

/**
 *
 * @author T540p
 */
public abstract class Employee {

    public final static double HIGH_PROBABILITY = 0.8;
    public final static double TYPICAL_PROBABILITY = 0.5;
    public final static double LOW_PROBABILITY = 0.1;
    public final static double VERY_LOW_PROBABILITY = 0.01;
    public final static double ZERO_PROBABILITY = 0.0;

    protected int employeeId;
    public int getId() {
        return employeeId;
    }
    protected int teamId;

    public int getTeamId() {
        return teamId;
    }

    protected boolean leader;

    public boolean isLeader() {
        return leader;
    }

    public Employee(int employeeId, int teamId, boolean leader) {
        this.employeeId = employeeId;
        this.teamId = teamId;
        this.leader = leader;
    }

    protected abstract double getInterSpeaking();

    protected abstract double getInterListening();

    protected abstract double getOuterSpeaking();

    protected abstract double getOuterListening();

    protected abstract double getOwnHistoryFactor();

    protected abstract double getNeighborBestFactor();

    protected double getNeighborBestFactorMin() {
        return 0.0;
    }

    public void aggregateNeighbours(Set<Integer> neighbours, double[][] best, double[] bestValues, double[][] current, double[] values) {
        int bestId = employeeId;
        double bestValue = bestValues[employeeId];
        for (int neighbourId : neighbours) {
            if (bestValue > bestValues[neighbourId]) {
                bestValue = bestValues[neighbourId];
                bestId = neighbourId;
            }
        }
        if (bestValue < attractionPointValue) {
            attractionPoint = Arrays.copyOf(best[bestId], best[bestId].length);
        }
    }

    protected double attractionPointValue = Double.POSITIVE_INFINITY;
    protected double[] attractionPoint;

    public double[] getAttractionPoint() {
        return attractionPoint;
    }

    public double probabilityOfGettingInformationFromCoworker(Employee other) {
        if (this.equals(other)) {
            return 1;
        }
        if (this.teamId == other.teamId) {
            return (other.getInterSpeaking() + this.getInterListening())
                    / ((this.leader || other.leader) ? 1.0 : 2.0);
        } else {
            return (other.getOuterSpeaking() + this.getOuterListening())
                    / ((this.leader && other.leader) ? 1.0 : 2.0);
        }
    }
}

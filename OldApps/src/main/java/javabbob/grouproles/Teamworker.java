/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob.grouproles;

import java.util.Arrays;
import java.util.Set;
import javabbob.Employee;

/**
 *
 * @author T540p
 */
public class Teamworker extends Employee {

    public Teamworker(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    @Override
    protected double getInterSpeaking() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getInterListening() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getOuterSpeaking() {
        return Employee.LOW_PROBABILITY;
    }

    @Override
    protected double getOuterListening() {
        return Employee.LOW_PROBABILITY;
    }

    @Override
    protected double getOwnHistoryFactor() {
        return 0.0;
    }

    @Override
    protected double getNeighborBestFactor() {
        return 1.4;
    }

    @Override
    public void aggregateNeighbours(Set<Integer> neighbours, double[][] best, double[] bestValues, double[][] current, double[] values) {
        attractionPoint = new double[best[employeeId].length];
        for (int neighbourId : neighbours) {
            for (int dimension = 0; dimension < attractionPoint.length; ++dimension) {
                //alternatively current
                attractionPoint[dimension] += best[neighbourId][dimension] / neighbours.size();
            }
        }
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob.grouproles;

import java.util.Arrays;
import java.util.Set;
import javabbob.Employee;

/**
 *
 * @author T540p
 */
public class ResourceInvestigator extends Employee {

    public ResourceInvestigator(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    @Override
    protected double getInterSpeaking() {
        return Employee.HIGH_PROBABILITY;
    }

    @Override
    protected double getInterListening() {
        return Employee.HIGH_PROBABILITY;
    }

    @Override
    protected double getOuterSpeaking() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getOuterListening() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getOwnHistoryFactor() {
        return 0.0;
    }

    @Override
    protected double getNeighborBestFactor() {
        return 0.2;
    }

    @Override
    protected double getNeighborBestFactorMin() {
        return 0.9;
    }

    @Override
    public void aggregateNeighbours(Set<Integer> neighbours, double[][] best, double[] bestValues, double[][] current, double[] values) {
//        int bestId = employeeId;
//        double bestValue = bestValues[employeeId];

        double maxDistance = Double.NEGATIVE_INFINITY;
        int furthestBest = -1;
        for (int neighbor : neighbours) {

            double distance = 0.0;
            for (int dim = 0; dim < best[employeeId].length; ++dim) {
                distance += (current[employeeId][dim] - best[neighbor][dim]) * (current[employeeId][dim] - best[neighbor][dim]);
            }
            if (distance > maxDistance) {
                maxDistance = distance;
                furthestBest = neighbor;
            }
//            if (bestValue > bestValues[neighbor]) {
//                bestValue = bestValues[neighbor];
//                bestId = neighbor;
//            }
        }

        attractionPoint = Arrays.copyOf(best[furthestBest], best[furthestBest].length);
    }

}

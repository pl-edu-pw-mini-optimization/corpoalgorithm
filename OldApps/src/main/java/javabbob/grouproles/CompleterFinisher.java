/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob.grouproles;

import javabbob.Employee;

/**
 *
 * @author T540p
 */
public class CompleterFinisher extends Employee {

    public CompleterFinisher(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    @Override
    protected double getInterSpeaking() {
        return Employee.LOW_PROBABILITY;
    }

    @Override
    protected double getInterListening() {
        return Employee.VERY_LOW_PROBABILITY;
    }

    @Override
    protected double getOuterSpeaking() {
        return Employee.ZERO_PROBABILITY;
    }

    @Override
    protected double getOuterListening() {
        return Employee.ZERO_PROBABILITY;
    }

    @Override
    protected double getOwnHistoryFactor() {
        return 0.0;
    }

    @Override
    protected double getNeighborBestFactor() {
        return 0.2;
    }
    
    @Override
    protected double getNeighborBestFactorMin() {
        return 0.9;
    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob.grouproles;

import java.util.Arrays;
import java.util.Set;
import javabbob.Employee;
import javabbob.ParticleSwarmOptimization;
import static javabbob.ParticleSwarmOptimization.rand;
import javabbob.SamplingEmployee;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

/**
 *
 * @author T540p
 */
public class Specialist extends SamplingEmployee {

    public Specialist(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    @Override
    protected double getInterSpeaking() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getInterListening() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getOuterSpeaking() {
        return Employee.ZERO_PROBABILITY;
    }

    @Override
    protected double getOuterListening() {
        return Employee.ZERO_PROBABILITY;
    }

    @Override
    protected double getOwnHistoryFactor() {
        return 0.0;
    }

    @Override
    protected double getNeighborBestFactor() {
        return 0.2;
    }

    @Override
    protected double getNeighborBestFactorMin() {
        return 0.9;
    }

    @Override
    public void aggregateNeighbours(Set<Integer> neighbours, double[][] best, double[] bestValues, double[][] current, double[] values) {
        final int dimensions = current[employeeId].length;
        gatherSamples(neighbours, current, values);
        if (clusterIterator == null || !clusterIterator.hasNext()) {
            performRandomClustering(neighbours);
            clusterIterator = clusterCenters.iterator();
        }
        CentroidCluster<VectorWithValue> chosen = clusterIterator.next();
        final int sampleSize = chosen.getPoints().size();
        attractionPoint = Arrays.copyOf(best[employeeId], dimensions);

        if (sampleSize > 2 * dimensions + 1) {
            double[] fdata = new double[sampleSize];
            double[][] xdata = new double[sampleSize][2 * dimensions + 1];
            int sampleId = 0;
            for (VectorWithValue sample : chosen.getPoints()) {
                fdata[sampleId] = sample.value;
                xdata[sampleId][2 * dimensions] = 1.0;
                for (int dim = 0; dim < dimensions; dim++) {
                    xdata[sampleId][dim] = sample.x[dim] * sample.x[dim];
                    xdata[sampleId][dim + dimensions] = sample.x[dim];
                }
                ++sampleId;
            }

            OLSMultipleLinearRegression lm = new OLSMultipleLinearRegression();
            lm.setNoIntercept(true);
            lm.newSampleData(fdata, xdata);
            double[] parameters = lm.estimateRegressionParameters();
            for (int i = 0; i < dimensions; ++i) {
                if (parameters[i] > 1e-8) {
                    attractionPoint[i] = -parameters[i + dimensions] / 2.0 / parameters[i];
                    if (attractionPoint[i] > 5.0) {
                        attractionPoint[i] = 5.0;
                    }
                    if (attractionPoint[i] < -5.0) {
                        attractionPoint[i] = -5.0;
                    }
                } else {
                    attractionPoint[i] = best[employeeId][i];
                }
            }
        }
    }

}

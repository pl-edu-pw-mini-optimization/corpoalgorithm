/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob.grouproles;

import javabbob.Employee;

/**
 *
 * @author T540p
 */
public class Shaper extends Employee {

    public Shaper(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    @Override
    protected double getInterSpeaking() {
        return Employee.HIGH_PROBABILITY;
    }

    @Override
    protected double getInterListening() {
        return Employee.HIGH_PROBABILITY;
    }

    @Override
    protected double getOuterSpeaking() {
        return Employee.LOW_PROBABILITY;
    }

    @Override
    protected double getOuterListening() {
        return Employee.LOW_PROBABILITY;
    }

    @Override
    protected double getOwnHistoryFactor() {
        return 1.4;
    }

    @Override
    protected double getNeighborBestFactor() {
        return 1.4;
    }
    
}

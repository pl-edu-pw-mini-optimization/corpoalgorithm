import java.io.*;
import java.net.*;
import java.util.*;


////////////////////////////////////////////////////////////
// black box library Java proxy
//
public class BBComp
{
    ////////////////////////////////////////////////////////////
    // simplistic network abstraction
    //

    private static final int          DEFAULT_PORT       = 7000;

    private Socket m_socket = null;
    private BufferedReader m_input = null;
    private PrintWriter m_output = null;

    public BBComp() throws Exception
    {
        m_socket = new Socket("localhost", DEFAULT_PORT);
        m_input = new BufferedReader(new InputStreamReader(m_socket.getInputStream()));
        m_output = new PrintWriter(m_socket.getOutputStream(), true);
    }

    public BBComp(int port) throws Exception
    {
        m_socket = new Socket("localhost", port);
        m_input = new BufferedReader(new InputStreamReader(m_socket.getInputStream()));
        m_output = new PrintWriter(m_socket.getOutputStream(), true);
    }

    public void close()
    {
        // disconnect and kill proxy
        try
        {
            if (m_socket != null) m_socket.close();
        }
        catch (Exception ex)
        {
        }
        finally
        {
            m_socket = null;
        }
    }

    public void finalize()
    {
        close();
    }

    private String receiveLine() throws Exception
    {
        return m_input.readLine();
    }

    private void sendLine(String line) throws Exception
    {
        m_output.print(line + "\n");
        m_output.flush();
    }


    ////////////////////////////////////////////////////////////
    // black box library interface functions
    //

    boolean configure(boolean history, String logfilepath) throws Exception
    {
        sendLine("configure(" + (history ? "1" : "0") + ",\"" + logfilepath + "\")");
        return (Integer.parseInt(receiveLine()) != 0);
    }

    boolean login(String username, String password) throws Exception
    {
        sendLine("login(\"" + username + "\", \"" + password + "\")");
        return (Integer.parseInt(receiveLine()) != 0);
    }

    int numberOfTracks() throws Exception
    {
        sendLine("numberOfTracks()");
        return Integer.parseInt(receiveLine());
    }

    String trackName(int trackindex) throws Exception
    {
        sendLine("trackName(" + new Integer(trackindex).toString() + ")");
        return receiveLine();
    }

    boolean resetTrack(String trackname) throws Exception
    {
        sendLine("resetTrack(" + trackname + ")");
        return (Integer.parseInt(receiveLine()) != 0);
    }

    boolean setTrack(String trackname) throws Exception
    {
        sendLine("setTrack(\"" + trackname + "\")");
        return (Integer.parseInt(receiveLine()) != 0);
    }

    int numberOfProblems() throws Exception
    {
        sendLine("numberOfProblems()");
        return Integer.parseInt(receiveLine());
    }

    boolean setProblem(int problemID) throws Exception
    {
        sendLine("setProblem(" + new Integer(problemID).toString() + ")");
        return (Integer.parseInt(receiveLine()) != 0);
    }

    int numberOfObjectives() throws Exception
    {
        sendLine("numberOfObjectives()");
        return Integer.parseInt(receiveLine());
    }

    int dimension() throws Exception
    {
        sendLine("dimension()");
        return Integer.parseInt(receiveLine());
    }

    int budget() throws Exception
    {
        sendLine("budget()");
        return Integer.parseInt(receiveLine());
    }

    int evaluations() throws Exception
    {
        sendLine("evaluations()");
        return Integer.parseInt(receiveLine());
    }

    Vector<Double> parseVector(String str, int start) throws Exception
    {
        assert(str.charAt(start) == '[');
        start++;
        int dim = 1;
        int end = 0;
        for (int i=start; i<str.length(); i++)
        {
            if (str.charAt(i) == ' ') dim++;
            if (str.charAt(i) == ']') { end = i; break; }
        }
        assert(end > 0);
        Vector<Double> ret = new Vector<Double>(dim);
        ret.setSize(dim);
        for (int i=0; i<dim; i++)
        {
            int finish = start;
            while (finish < str.length())
            {
                finish++;
                if (str.charAt(finish) == ' ' || str.charAt(finish) == ']') break;
            }
            String token = str.substring(start, finish);
            ret.setElementAt(Double.parseDouble(token), i);
            start = finish + 1;
        }
        return ret;
    }

    public class EvaluateReturn
    {
        public boolean result;
        public Vector<Double> value;
    }

    EvaluateReturn evaluate(Vector<Double> point) throws Exception
    {
        String command = "evaluate([" + point.elementAt(0).toString();
        for (int i=1; i<point.size(); i++)
        {
            command += " ";
            command += point.elementAt(i).toString();
        }
        command += "])";
        sendLine(command);
        String result = receiveLine();
        EvaluateReturn ret = new EvaluateReturn();
        if (result.equals("0"))
        {
            ret.result = false;
            return ret;
        }
        else
        {
            ret.result = true;
            if (result.charAt(2) == '[')
            {
                ret.value = parseVector(result, 2);
            }
            else
            {
                ret.value = new Vector<Double>(1);
                ret.value.setSize(1);
                ret.value.setElementAt(Double.parseDouble(result.substring(2)), 0);
            }
            return ret;
        }
    }

    public class HistoryReturn
    {
        public boolean result;
        public Vector<Double> point;
        public Vector<Double> value;
    }

    HistoryReturn history(int index) throws Exception
    {
        sendLine("history(" + new Integer(index).toString() + ")");
        String result = receiveLine();
        HistoryReturn ret = new HistoryReturn();
        if (result.equals("0"))
        {
            ret.result = false;
            return ret;
        }
        else
        {
            assert(result.substring(0, 3).equals("1,["));
            ret.result = true;
            ret.point = parseVector(result, 2);
            int pos = 2;
            while (pos < result.length())
            {
                if (result.charAt(pos) == ',') break;
                pos++;
            }
            pos++;
            if (result.charAt(pos) == '[')
            {
                ret.value = parseVector(result, pos);
            }
            else
            {
                ret.value = new Vector<Double>(1);
                ret.value.setSize(1);
                ret.value.setElementAt(Double.parseDouble(result.substring(pos)), 0);
            }
            return ret;
        }
    }

    String errorMessage() throws Exception
    {
        sendLine("errorMessage()");
        return receiveLine();
    }
}

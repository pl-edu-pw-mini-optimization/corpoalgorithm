package pl.edu.pw.mini.zmog.pso.velocity;

import java.util.*;
import lombok.Getter;
import org.apache.commons.math3.util.MathArrays;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.LocalNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;

/**
 *
 *
 * @since 2017-12-02, 20:16:06
 * @author Mateusz Uliński
 */
public class StandardVelocityUpdate implements VelocityUpdateRule {
    private static double w = 0.9;
    private static double c1 = 1.2;
    private static double c2 = 1.2;

    @Getter
    private static StandardVelocityUpdate instance = new StandardVelocityUpdate();

    @Override
    public double[] calculate(Particle p, Neighbourhood neighbourhood, MemoryManager memoryManager) {
        double[] x = p.x;
        double[] bestX = p.bestX;
        double[] velocity = p.velocity;
        double[] myD = MathArrays.ebeSubtract(bestX, x);
        double[] myBest = MathArrays.ebeMultiply(myD, RandomHelper.uniform(0, c1, x.length));

        double[] swarmD = MathArrays.ebeSubtract(neighbourhood.getBestInSwarm(p).bestX, x);
        double[] swarmBest = MathArrays.ebeMultiply(swarmD, RandomHelper.uniform(0, c2, x.length));
        velocity = MathArrays.scale(w, velocity);
        velocity = MathArrays.ebeAdd(velocity, myBest);
        velocity = MathArrays.ebeAdd(velocity, swarmBest);
        return velocity;
    }
    @Override
    public Neighbourhood getStandardTopology() {
        return LocalNeighbourhood.getInstance();
    }
}

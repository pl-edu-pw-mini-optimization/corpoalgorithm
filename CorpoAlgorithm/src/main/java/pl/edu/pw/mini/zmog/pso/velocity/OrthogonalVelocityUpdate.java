package pl.edu.pw.mini.zmog.pso.velocity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;
import lombok.Getter;
import org.apache.commons.math3.util.MathArrays;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.LocalNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;

/**
 *
 *
 * @since 2017-12-18, 06:53:36
 * @author Mateusz Uliński
 */
public class OrthogonalVelocityUpdate implements VelocityUpdateRule {

    @Getter
    private static OrthogonalVelocityUpdate instance = new OrthogonalVelocityUpdate();
    private static double w = 0.9;
    private static double c = 0.2;
    private int maxStagnation = 5;
    public static final Map<Integer, int[][]> OAs = new HashMap<>();

    static { //initOA for popular dimensions

        int[] dim = IntStream.range(2, 51).toArray();
        for (int i : dim)
            OAs.put(i, calculateOA(i));

    }

    @Override
    public double[] calculate(Particle p, Neighbourhood neighbourhood, MemoryManager memoryManager) {
        p.stagnation++;
        double[] x = p.x;
        double[] bestX = p.bestX;
        double[] velocity = p.velocity;
        if (p.P0 == null || p.stagnation > maxStagnation) {
            p.stagnation = 0;
            p.P0 = oed(p, neighbourhood.getBestInNeighbourhood(p));

        }

        double[] myD = MathArrays.ebeSubtract(construct(p.P0), x);
        double[] myBest = MathArrays.ebeMultiply(myD, RandomHelper.uniform(0, c, x.length));
        velocity = MathArrays.scale(w, velocity);
        velocity = MathArrays.ebeAdd(velocity, myBest);
        return velocity;
    }

    private static int[][] calculateOA(int dim) {
        int u = 32 - Integer.numberOfLeadingZeros(dim);//ceil(log2 (dim +1)) - patrz javadoc numberOfLeadingZeros
        int m = 1 << u;
        int n = m - 1;
        int[][] l = new int[m][n];

        for (int a = 1; a <= m; a++)
            for (int k = 1; k <= u; k++) {
                int b = 1 << k - 1;
                int c = (a - 1) / (1 << u - k);//floor
                l[a - 1][b - 1] = c % 2;
            }
        for (int a = 1; a <= m; a++)
            for (int k = 2; k <= u; k++) {
                int b = 1 << k - 1;
                for (int s = 1; s <= b - 1; s++) {
                    int c = l[a - 1][s - 1] + l[a - 1][b - 1];//floor
                    l[a - 1][b + s - 1] = c % 2;
                }
            }
        for (int[] d : l)
            for (int i = 0; i < d.length; i++)
                d[i]++;
        return l;
    }

    private Particle[] oed(Particle thisParticle, Particle bestInNeibourhood) {//jedna z czastek to nasza, druga to wybrana najlepsza z sasiedztwa
        int[][] oa = OAs.get(thisParticle.x.length);//pobieramy macierz OA (wczensiej policzona i zcacheowana)
//        Particle[][] memory = new Particle[oa.length][];
        double[] cacheValues = new double[oa.length];
        Particle[] best = null;//najlepsza kombinacja wyliczona z OA
        Double bestval = null;
        //orthogonal array
        for (int k = 0; k < oa.length; k++) {
            int[] selector = oa[k];
            Particle[] p = new Particle[thisParticle.x.length];
            for (int i = 0; i < thisParticle.x.length; i++)
                p[i] = selector[i] == 1 ? thisParticle : bestInNeibourhood;
            double[] x = construct(p);
            double val = thisParticle.getF().value(x);
            if (bestval == null || val < bestval) {
                bestval = val;
                best = p;
            }
//            memory[k] = p;
            cacheValues[k] = val;
        }
        //factor analysis
        //test factors are the D dimensions and the levels in each factor are 2 for choosing from P i or P n
        double[][] s = new double[best.length][2];
        for (int n = 0; n < best.length; n++) //factors
            for (int q = 1; q < 3; q++) {//levels
                double snq = 0;
                int znq = 0;
                final int fq = q;
                for (int m = 0; m < oa.length; m++) {
                    int z = oa[m][n] == q ? 1 : 0;
                    snq += cacheValues[m] * z;
                    znq += z;
                }
                s[n][q - 1] = snq / znq;
            }
        Particle[] pp = new Particle[best.length];
        for (int i = 0; i < pp.length; i++)
            pp[i] = s[i][0] < s[i][1] ? thisParticle : bestInNeibourhood;//wydaje mi sie ze tak, ale moze odwrotnie
        double[] xp = construct(pp);//twporzymy wektor x, na podstawie wektora czastek
        double value = thisParticle.getF().value(xp);
        return value < bestval ? pp : best;//wybieramy 
    }

    private double[] construct(Particle[] p) {
        double[] d = new double[p.length];

        for (int i = 0; i < d.length; i++)
            d[i] = p[i].bestX[i];
        return d;
    }

    public static void main(String[] args) {
        for (int i = 2; i <9; i++) {
            System.out.println("i = " + i);
            int[][] get = OAs.get(i);
            for (int[] row : get)
                System.out.println(Arrays.toString(row));
            System.out.println("");

        }
    }
    @Override
    public Neighbourhood getStandardTopology() {
        return LocalNeighbourhood.getInstance();
    }
}

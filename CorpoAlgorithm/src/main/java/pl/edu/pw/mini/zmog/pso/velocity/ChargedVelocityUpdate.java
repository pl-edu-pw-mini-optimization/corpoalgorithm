/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pw.mini.zmog.pso.velocity;

import java.util.List;
import lombok.Getter;
import org.apache.commons.math3.util.MathArrays;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.particles.ChargedParticle;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;

/**
 *
 * @author azych
 */
public class ChargedVelocityUpdate implements VelocityUpdateRule {
    private static double w = 0.9;
    private static double c = 1.2;
    private static double pcore = 1;
    private static double pmax = 100;

    @Getter
    private static ChargedVelocityUpdate instance = new ChargedVelocityUpdate();

    @Override
    public double[] calculate(Particle p, Neighbourhood neighbourhood, MemoryManager memoryManager) {
        double[] x = p.x;
        double[] bestX = p.bestX;
        double[] velocity = p.velocity;
        double[] myD = MathArrays.ebeSubtract(bestX, x);
        double[] myBest = MathArrays.ebeMultiply(myD, RandomHelper.uniform(0, c, x.length));

        double[] swarmD = MathArrays.ebeSubtract(neighbourhood.getBestInSwarm(p).bestX, x);
        double[] swarmBest = MathArrays.ebeMultiply(swarmD, RandomHelper.uniform(0, c, x.length));
        velocity = MathArrays.scale(w, velocity);
        velocity = MathArrays.ebeAdd(velocity, myBest);
        velocity = MathArrays.ebeAdd(velocity, swarmBest);

        //powyzsza czesc identyczna jak w StandardPSO, ponizej zaczyna sie czesc wlasciwa dla ChargedPSO
        //liczenie czynnika "odpychajacego" miedzy naladowanymi czasteczkami
        List<Particle> neighbours = neighbourhood.getNeighbours(p);

        double[] a = new double[x.length];
        for (int i = 0; i < neighbours.size(); i++) {
            Particle neighbour = neighbours.get(i);
            double q = p.charge * neighbour.charge;
            double r = MathArrays.distance(x, neighbour.x);

            if (r < pcore || r > pmax)
                continue;

            double[] rVector = MathArrays.ebeSubtract(x, neighbour.x);
            rVector = MathArrays.scale(q / (r * r * r), rVector);

            a = MathArrays.ebeAdd(a, rVector);
        }

        velocity = MathArrays.ebeAdd(velocity, a);

        return velocity;
    }

    @Override
    public Neighbourhood getStandardTopology() {
        return FullNeighbourhood.getInstance();
    }
}

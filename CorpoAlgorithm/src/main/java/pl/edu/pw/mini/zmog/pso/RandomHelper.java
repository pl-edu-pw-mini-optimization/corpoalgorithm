package pl.edu.pw.mini.zmog.pso;

import java.util.*;
import lombok.Getter;
import lombok.Setter;

/**
 *
 *
 * @since 2017-12-02, 15:57:46
 * @author Mateusz Uliński
 */
public class RandomHelper {
    @Setter
    @Getter
    static Random random = new Random(0);

    public static double[] getRandom(int dim) {
        return random.doubles().limit(dim).toArray();
    }

    public static double[] uniform(double min, double max, int dim) {

        double[] x = new double[dim];
        for (int i = 0; i < x.length; i++)
            x[i] = random.nextDouble() * (max - min) + min;
        return x;
    }

    public static double uniform(double min, double max) {
        return random.nextDouble() * (max - min) + min;
    }

    public static double[] normalizedCummulative(int dim) {
        double[] uniform = uniform(0, 1, dim);
        double d = 0;
        for (int i = 0; i < uniform.length; i++)
            d += uniform[i];
        for (int i = 0; i < uniform.length; i++)
            uniform[i] /= d;
        d = 0;
        for (int i = 0; i < uniform.length; i++) {
            uniform[i] += d;
            d = uniform[i];
        }
        uniform[uniform.length - 1] = 1;
        return uniform;
    }

    public static void main(String[] args) {
        List<Integer> l = Arrays.asList(0, 1, 2);
        double[] prob = {0.33, 0.66, 1};
        int[] counts = {0, 0, 0};
        for (int i = 0; i < 1000; i++)
            counts[chooseRandom(l, prob)]++;
        System.out.println(Arrays.toString(counts));
    }

    /**
     *
     * @param <T>
     * @param objects
     * @param probabilitiesCummulative for i-th object
     *                                 probabilitiesCummulative[i] = sum k=0..i p[k]
     * @return
     */
    public static <T> T chooseRandom(List<T> objects, double[] probabilitiesCummulative) {
        double x = random.nextDouble();
        for (int i = 0; i < probabilitiesCummulative.length; i++) {
            double d = probabilitiesCummulative[i];
            if (x < d)
                return objects.get(i);
        }
        return objects.get(0);
    }
}

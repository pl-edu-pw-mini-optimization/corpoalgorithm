/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pw.mini.zmog.pso.particles;

import pl.edu.pw.mini.zmog.pso.swarms.Swarm;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.LocalNeighbourhood;
import pl.edu.pw.mini.zmog.pso.velocity.ChargedVelocityUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.UnifiedVelocityUpdate;

/**
 *
 * @author azych
 */
public class UnifiedParticle extends CompositeParticle {
    
    public UnifiedParticle(double[] x, double currentValue, double[] velocity, Swarm swarm) {
        super(new UnifiedVelocityUpdate(), x, currentValue, velocity, swarm);
        
        this.setNeighbourhood(new LocalNeighbourhood());
    }
}

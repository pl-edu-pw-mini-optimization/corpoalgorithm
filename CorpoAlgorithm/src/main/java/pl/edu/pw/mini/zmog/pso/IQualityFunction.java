package pl.edu.pw.mini.zmog.pso;

import java.util.*;
import java.util.function.Function;
import java.util.stream.DoubleStream;
import lombok.Getter;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.zmog.pso.coco.Problem;

/**
 *
 *
 * @since 2017-12-02, 15:02:54
 * @author Mateusz Uliński
 */
public class IQualityFunction {
    private Function<double[], Double> function;
    @Getter
    private double[] lowerBounds;
    @Getter
    private double[] upperBounds;
    @Getter
    private String name;
    @Getter
    private Integer dim;

    public IQualityFunction(Problem problem) {
        this.function = x -> problem.evaluateFunction(x)[0];
        this.lowerBounds = problem.getSmallestValuesOfInterest();
        this.upperBounds = problem.getLargestValuesOfInterest();
        this.dim = problem.getDimension();
    }

    public IQualityFunction(Function<double[], Double> function, int dim, double min, double max) {
        this.function = function;
        this.lowerBounds = DoubleStream.iterate(min, x -> x).limit(dim).toArray();
        this.upperBounds = DoubleStream.iterate(max, x -> x).limit(dim).toArray();
        this.dim = dim;
    }
    
    public IQualityFunction(Function<double[], Double> function, int dim, double min, double max, String name) {
        this.function = function;
        this.lowerBounds = DoubleStream.iterate(min, x -> x).limit(dim).toArray();
        this.upperBounds = DoubleStream.iterate(max, x -> x).limit(dim).toArray();
                this.dim = dim;
        this.name = name;
    }

    public double value(double[] x) {
        return function.apply(x);
    }

    public Pair<double[], Double> createInitialSolution(Random generator) {
        double[] x = new double[dim];
        for (int j = 0; j < dim; j++) {
            double range = upperBounds[j] - lowerBounds[j];
            x[j] = lowerBounds[j] + generator.nextDouble() * range;
        }
        return Pair.create(x, value(x));
    }

    public double[] createInitialVelocity(Random generator) {
        double[] v = new double[dim];
        for (int j = 0; j < dim; j++) {
            double range = upperBounds[j] - lowerBounds[j];
            v[j] = range * generator.nextGaussian() / 100;
        }
        return v;
    }

}

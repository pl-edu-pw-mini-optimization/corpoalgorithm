package pl.edu.pw.mini.zmog.pso.particles;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import pl.edu.pw.mini.zmog.pso.ParticleSwarmOptimization;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.swarms.Swarm;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;
import pl.edu.pw.mini.zmog.pso.velocity.StandardVelocityUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.VelocityUpdateRule;

/**
 *
 *
 * @since Dec 16, 2017, 5:45:03 PM
 * @author Mateusz Uliński
 */
public class ProbabilisticParticle extends CompositeParticle {
    private List<VelocityUpdateRule> velocityRules;
    public double[] velocityProbabilities;
    private List<Neighbourhood> topologies;
    private double[] topologyProbabilities;

    private int maxEvaluations = 10*ParticleSwarmOptimization.TURNS_PER_SWARM;
    private int counter = maxEvaluations;

    public ProbabilisticParticle(double[] x, double currentValue, double[] velocity, Swarm swarm) {
        super(StandardVelocityUpdate.getInstance(), x, currentValue, velocity, swarm);
    }

    @Override
    protected void updateVelocity() {
        if (counter++ == maxEvaluations) {
            counter = 0;
            updateStrategy();
        }
        super.updateVelocity();
    }

    public void setVelocityRules(List<VelocityUpdateRule> velocityRules, double[] velocityProbabilities) {
        this.velocityRules = velocityRules;
        this.velocityProbabilities = velocityProbabilities;
    }

    public void setNeighbourhoodTopologies(List<Neighbourhood> topologies, double[] topologyProbabilities) {
        this.topologies = topologies;
        this.topologyProbabilities = topologyProbabilities;
    }

    protected void updateStrategy() {
        setVelocityUpdateRule(RandomHelper.chooseRandom(velocityRules, velocityProbabilities));
    }

    public void adjust(int indexToReduce, int indexToIncrease) {
        double val;
        if (indexToReduce == 0)
            val = velocityProbabilities[0];
        else
            val = velocityProbabilities[indexToReduce] - velocityProbabilities[indexToReduce - 1];
        val = RandomHelper.uniform(0, val);
        for (int i = indexToReduce; i < velocityProbabilities.length; i++)
            velocityProbabilities[i] -= val;
        for (int i = indexToIncrease; i < velocityProbabilities.length; i++)
            velocityProbabilities[i] += val;
    }
   
    public void adjust(Map<Integer, Double> adjustMap) {
        
        Integer n = adjustMap.size();
        
        Double sum = 0.0;
        for (Map.Entry<Integer, Double> e : adjustMap.entrySet()) {
            sum += e.getValue();
        }
        
        if (sum == 0)
            velocityProbabilities = RandomHelper.normalizedCummulative(velocityRules.size());
        for (int i=0; i<n; i++) {
            if (i == 0)
                velocityProbabilities[i] = adjustMap.get(i)/sum;
            else
                velocityProbabilities[i] = velocityProbabilities[i-1] + adjustMap.get(i)/sum;
        }
    }
}

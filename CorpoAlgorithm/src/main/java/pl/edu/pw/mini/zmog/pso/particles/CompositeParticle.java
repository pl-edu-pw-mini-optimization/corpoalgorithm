package pl.edu.pw.mini.zmog.pso.particles;

import java.util.stream.DoubleStream;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.math3.util.MathArrays;
import pl.edu.pw.mini.zmog.pso.swarms.Swarm;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;
import pl.edu.pw.mini.zmog.pso.velocity.VelocityUpdateRule;

/**
 *
 *
 * @since 2017-12-02, 13:42:58
 * @author Mateusz Uliński
 */
@Getter
public class CompositeParticle extends Particle {
    private static MemoryManager defaultMemoryManager = new MemoryManager() {
    };
    private static Neighbourhood defaultNeighbourhood = new Neighbourhood() {
    };
    @Setter
    private MemoryManager memoryManager;
    @Setter
    private Neighbourhood neighbourhood;
    @Setter
    private VelocityUpdateRule velocityUpdateRule;

    public int failsToReset;
    public int maxFailsToReset = 15;

    public CompositeParticle(VelocityUpdateRule rule, double[] x, double currentValue, double[] velocity, Swarm swarm) {
        super(x, currentValue, velocity, swarm);
        this.velocityUpdateRule = rule;
    }

    @Override
    protected void updateVelocity() {
        Neighbourhood neighbourhood = this.neighbourhood;
        if (neighbourhood == null)
            neighbourhood = velocityUpdateRule.getStandardTopology();
        MemoryManager memoryManager = this.memoryManager;
        if (memoryManager == null)
            memoryManager = defaultMemoryManager;
        velocity = velocityUpdateRule.calculate(this, neighbourhood, memoryManager);
    }

    @Override
    protected boolean needToReset() {
        Neighbourhood neighbourhood = this.neighbourhood;
        if (getSwarm().getBest() == this)
            return false;
        if (neighbourhood == null)
            neighbourhood = defaultNeighbourhood;
        if (neighbourhood.getBestInNeighbourhood(this) != this || DoubleStream.of(velocity).map(x -> x * x).sum() > 1) {
            failsToReset = 0;
            return false;

        }
        if (failsToReset++ == maxFailsToReset) {
//            System.out.println("reset");
            failsToReset = 0;
            return true;
        }
        return false;
    }

}

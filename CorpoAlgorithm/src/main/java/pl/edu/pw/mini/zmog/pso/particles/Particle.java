package pl.edu.pw.mini.zmog.pso.particles;

import java.util.Random;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.zmog.pso.IQualityFunction;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.swarms.Swarm;

/**
 *
 *
 * @since 2017-12-02, 13:34:03
 * @author Mateusz Uliński
 */
@ToString(exclude = {"swarm", "f", "P0"})
public abstract class Particle {
    public double[] x;
    public double currentValue;
    public double[] velocity;
    public double[] bestX;
    public double bestValue;

    @Getter
    private Swarm swarm;

    //orthongonal
    public Particle[] P0;
    public int stagnation;
    //charged
    public double charge = 32;
    public double success = 0;
    private double maxFactor = 0.2;

    @Getter//use it wisely
    private IQualityFunction f;
    private final double[] vMax;

    public Particle(double[] x, double currentValue, double[] velocity, Swarm swarm) {
        this.x = x;
        this.currentValue = currentValue;
        this.velocity = velocity;
        this.swarm = swarm;
        this.f = swarm.getPso().getFunction();
        vMax = MathArrays.ebeSubtract(f.getUpperBounds(), f.getLowerBounds());
        MathArrays.scaleInPlace(maxFactor, vMax);

        bestValue = currentValue;
        bestX = x;
        if (RandomHelper.getRandom().nextDouble() > 0.5)
            charge = 0;
        else
            charge = 32;
    }

    protected abstract void updateVelocity();

    public void update() {
        updateVelocity();
        double[] v = velocity;
        for (int i = 0; i < v.length; i++)
            if (Math.abs(v[i]) > vMax[i])
                v[i] = vMax[i] * Math.signum(v[i]);
        x = MathArrays.ebeAdd(x, v);
        double lastValue = currentValue;
        currentValue = f.value(x);
        if (lastValue == 0 || currentValue > lastValue)
            success = 0;
        else
            success = (lastValue - currentValue) / lastValue;
            
        if (currentValue < bestValue) {
            bestValue = currentValue;
            bestX = x;
        }
        if (needToReset())
            reset();

    }

    protected boolean needToReset() {
        return false;
    }

    private void reset() {
        Random rand = swarm.getPso().getRandomGenerator();
        Pair<double[], Double> solution = f.createInitialSolution(rand);
        bestX = x = solution.getFirst();
        bestValue = currentValue = solution.getSecond();
        velocity = f.createInitialVelocity(rand);
    }

    public void VNS(double step, int tries) {
        IQualityFunction f = swarm.getPso().getFunction();
        int dim = f.getDim();
        Random rand = swarm.getPso().getRandomGenerator();
        double[] direction = new double[dim];
        double[] newDirection = new double[dim];
        double[] testPoint = new double[dim];
        double testValue;
        int missedTries = 2 * dim;
        for (int iterations = 0; iterations < dim * tries; ++iterations) {
            for (int i = 0; i < dim; ++i) {
                newDirection[i] = rand.nextGaussian() * step + direction[i] / 2;
                testPoint[i] = bestX[i] + newDirection[i];
            }
            testValue = f.value(testPoint);
            if (testValue <= bestValue) {
                for (int i = 0; i < dim; ++i) {
                    bestX[i] = testPoint[i];
                    direction[i] = newDirection[i];
                    missedTries = 2 * dim;
                }
                bestValue = testValue;
                bestX = testPoint;
            } else {
                for (int i = 0; i < dim; ++i)
                    testPoint[i] = bestX[i] - newDirection[i];
                testValue = f.value(testPoint);
                ++iterations;
                if (testValue <= bestValue) {
                    for (int i = 0; i < dim; ++i) {
                        bestX[i] = testPoint[i];
                        direction[i] = -newDirection[i];
                        missedTries = 2 * dim;
                    }
                    bestValue = testValue;
                } else {
                    if (--missedTries <= 0) {
                        missedTries = 2 * dim;
                        step /= 1.2;
                    };
                }
            }
            velocity = MathArrays.copyOf(direction);
            x = MathArrays.copyOf(testPoint);
        }
    }

    public void initVelocity() {
        Particle p = swarm.getParticles().get(RandomHelper.getRandom().nextInt(swarm.getParticles().size()));
        velocity = MathArrays.scale(0.5, MathArrays.ebeSubtract(p.x, x));
    }

}

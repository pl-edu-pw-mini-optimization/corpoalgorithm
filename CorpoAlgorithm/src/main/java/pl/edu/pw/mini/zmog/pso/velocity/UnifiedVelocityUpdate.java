/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pw.mini.zmog.pso.velocity;

import lombok.Getter;
import org.apache.commons.math3.util.MathArrays;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;

/**
 *
 * @author azych
 */
public class UnifiedVelocityUpdate implements VelocityUpdateRule {

    private static double w = 0.9;
    private static double c = 1.2;
    private static double u = 0.5;

    @Getter
    private static UnifiedVelocityUpdate instance = new UnifiedVelocityUpdate();

    @Override
    public double[] calculate(Particle p, Neighbourhood neighbourhood, MemoryManager memoryManager) {
        double[] x = p.x;
        double[] bestX = p.bestX;
        double[] velocity = p.velocity;
        double[] myD = MathArrays.ebeSubtract(bestX, x);
        double[] myBest = MathArrays.ebeMultiply(myD, RandomHelper.uniform(0, c, x.length));

        double[] swarmDLocal = MathArrays.ebeSubtract(neighbourhood.getBestInSwarm(p).bestX, x);
        double[] swarmBestLocal = MathArrays.ebeMultiply(swarmDLocal, RandomHelper.uniform(0, c, x.length));
        swarmBestLocal = MathArrays.scale(1 - u, swarmBestLocal);

        double[] swarmDGlobal = MathArrays.ebeSubtract(p.getSwarm().getBest().bestX, x);
        double[] swarmBestGlobal = MathArrays.ebeMultiply(swarmDGlobal, RandomHelper.uniform(0, c, x.length));
        swarmBestGlobal = MathArrays.scale(u, swarmBestGlobal);

        velocity = MathArrays.scale(w, velocity);
        velocity = MathArrays.ebeAdd(velocity, myBest);
        velocity = MathArrays.ebeAdd(velocity, swarmBestLocal);
        velocity = MathArrays.ebeAdd(velocity, swarmBestGlobal);
        return velocity;
    }

    @Override
    public Neighbourhood getStandardTopology() {
        return FullNeighbourhood.getInstance();
    }
}

package pl.edu.pw.mini.zmog.pso.coco;

import java.util.Arrays;
import java.util.Random;
import pl.edu.pw.mini.zmog.pso.IQualityFunction;
import pl.edu.pw.mini.zmog.pso.ParticleSwarmOptimization;
import pl.edu.pw.mini.zmog.pso.RandomHelper;

/**
 * An example of benchmarking random search on a COCO suite.
 * <p>
 * Set the parameter BUDGET_MULTIPLIER to suit your needs.
 */
public class ExampleExperiment {

    /**
     * The maximal budget for evaluations done by an optimization algorithm
     * equals dimension * BUDGET_MULTIPLIER. Increase the budget multiplier
     * value gradually to see how it affects the runtime.
     */
    public static int BUDGET_MULTIPLIER = (int)1e6;

    /**
     * The maximal number of independent restarts allowed for an algorithm that
     * restarts itself.
     */
    public static final int INDEPENDENT_RESTARTS = 100;
//
//    /**
//     * The random seed. Change if needed.
//     */
//    public static final long RANDOM_SEED = 0xdeadbeef;

    /**
     * The problem to be optimized (needed in order to simplify the interface
     * between the optimization algorithm and the COCO platform).
     */
    public static Problem PROBLEM;
    private static String[] arg;

    /**
     * Interface for function evaluation.
     */
    public interface Function {
        double[] evaluate(double[] x);
    }

    /**
     * Evaluate the static PROBLEM.
     */
    public static final Function evaluateFunction = new Function() {
        public double[] evaluate(double[] x) {
            return PROBLEM.evaluateFunction(x);
        }
    };

    /**
     * The main method initializes the random number generator and calls the
     * example experiment on the bi-objective suite.
     */
    public static void main(String[] args) {
        arg = args;
        Random randomGenerator = RandomHelper.getRandom();

        /* Change the log level to "warning" to get less output */
        CocoJNIReflectionWrapper.cocoSetLogLevel("info");

        System.out.println("Running the example experiment... (might take time, be patient)");
        System.out.flush();

        /* Start the actual experiments on a test suite and use a matching logger, for
         * example one of the following:
         *
         *   bbob                 24 unconstrained noiseless single-objective functions
         *   bbob-biobj           55 unconstrained noiseless bi-objective functions
         *   bbob-biobj-ext       92 unconstrained noiseless bi-objective functions
         *   bbob-largescale      24 unconstrained noiseless single-objective functions in large dimension
         *
         * Adapt to your need. Note that the experiment is run according
         * to the settings, defined in exampleExperiment(...) below.
         */
        exampleExperiment("bbob", "bbob", randomGenerator);

        System.out.println("Done!");
        System.out.flush();

        return;
    }

    /**
     * A simple example of benchmarking random search on a given suite with
     * default instances that can serve also as a timing experiment.
     *
     * @param suiteName       Name of the suite (e.g. "bbob" or "bbob-biobj").
     * @param observerName    Name of the observer matching with the chosen
     *                        suite
     *                        (e.g. "bbob-biobj" when using the "bbob-biobj-ext" suite).
     * @param randomGenerator The random number generator.
     */
    public static void exampleExperiment(String suiteName, String observerName, Random randomGenerator) {
        try {

            if (arg.length > 3) {
                BUDGET_MULTIPLIER = Integer.decode(arg[3]);
            }
            /* Set some options for the observer. See documentation for other options. */
            final String observerOptions
                    = "result_folder: " + arg[0] + "_" + Math.log10(BUDGET_MULTIPLIER) + "_on_" + suiteName + " "
                      + "algorithm_name: " + arg[0] + " "
                      + "algorithm_info: \"" + arg[0] + "\"";

            /* Initialize the suite and observer.
             * For more details on how to change the default options, see
             * http://numbbo.github.io/coco-doc/C/#suite-parameters and
             * http://numbbo.github.io/coco-doc/C/#observer-parameters. */
            Suite suite = new Suite(suiteName, "", "");
            Observer observer = new Observer(observerName, observerOptions);
            Benchmark benchmark = new Benchmark(suite, observer);

            /* Initialize timing */
            Timing timing = new Timing();
            int[] dim = null;
            if (arg.length > 1 && !"all".equals(arg[1])) {
                String[] dims = arg[1].split(",");
                dim = new int[dims.length];
                for (int i = 0; i < dims.length; i++) {
                    dim[i] = Integer.decode(dims[i]);
                }
            }
            String function[] = null;
            if (arg.length > 2 && !"all".equals(arg[2])) {
                function = (arg[2].split(","));
            }
            /* Iterate over all problems in the suite */
            while ((PROBLEM = benchmark.getNextProblem()) != null) {
                System.out.println("new problem " + PROBLEM.getName());
                int dimension = PROBLEM.getDimension();
                if (dim != null && Arrays.binarySearch(dim, dimension) < 0)
                    continue;
                if (function != null) {
                    boolean skip = true;
                    for (String fName : function) {
                        skip &= !PROBLEM.getName().contains(" " + fName + " ");
                        }
                    if (skip) continue;
                }
                /* Run the algorithm at least once */
                for (int run = 1; run <= 1 + INDEPENDENT_RESTARTS; run++) {

                    long evaluationsDone = PROBLEM.getEvaluations();
                    long evaluationsRemaining = (long) (dimension * BUDGET_MULTIPLIER) - evaluationsDone;

                    /* Break the loop if the target was hit or there are no more remaining evaluations */
                    if (PROBLEM.isFinalTargetHit() || (evaluationsRemaining <= 0))
                        break;

                    /* Call the optimization algorithm for the remaining number of evaluations */
                    myGAPSOSearch(evaluateFunction,
                                   dimension,
                                   PROBLEM.getNumberOfObjectives(),
                                   PROBLEM.getSmallestValuesOfInterest(),
                                   PROBLEM.getLargestValuesOfInterest(),
                                   evaluationsRemaining,
                                   randomGenerator);

                    /* Break the loop if the algorithm performed no evaluations or an unexpected thing happened */
                    if (PROBLEM.getEvaluations() == evaluationsDone) {
                        System.out.println("WARNING: Budget has not been exhausted (" + evaluationsDone + "/"
                                           + dimension * BUDGET_MULTIPLIER + " evaluations done)!\n");
                        break;
                    } else if (PROBLEM.getEvaluations() < evaluationsDone)
                        System.out.println("ERROR: Something unexpected happened - function evaluations were decreased!");
                }

                /* Keep track of time */
                timing.timeProblem(PROBLEM);
            }

            /* Output the timing data */
            timing.output();

            benchmark.finalizeBenchmark();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * A simple random search algorithm that can be used for single- as well as
     * multi-objective optimization.
     */
    public static void myGAPSOSearch(Function f,
                                      int dimension,
                                      int numberOfObjectives,
                                      double[] lowerBounds,
                                      double[] upperBounds,
                                      long maxBudget,
                                      Random randomGenerator) {

        ParticleSwarmOptimization pso = new ParticleSwarmOptimization(new IQualityFunction(PROBLEM), maxBudget, randomGenerator);
        if (arg.length > 0 && !"all".equals(arg[0])) {
            pso.init(arg[0]);
            pso.optimize(null);
        } else {
            pso.init();
            pso.optimize(null);
        }
    }
}

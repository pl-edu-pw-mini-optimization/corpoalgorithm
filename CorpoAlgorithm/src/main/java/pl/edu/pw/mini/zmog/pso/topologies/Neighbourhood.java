package pl.edu.pw.mini.zmog.pso.topologies;

import java.util.*;
import pl.edu.pw.mini.zmog.pso.particles.Particle;

/**
 *
 *
 * @since 2017-12-02, 13:34:41
 * @author Mateusz Uliński
 */
public interface Neighbourhood {
    public default List<Particle> getNeighbours(Particle p) {
        return Collections.EMPTY_LIST;
    }

    public default Particle getBestInNeighbourhood(Particle p) {
        return p;
    }

    public default List<Particle> getSwarm(Particle p) {
        return p.getSwarm().getParticles();
    }

    public default Particle getBestInSwarm(Particle p) {
        return p.getSwarm().getBest();
    }

}

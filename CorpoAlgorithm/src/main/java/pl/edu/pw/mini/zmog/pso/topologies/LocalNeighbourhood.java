/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pw.mini.zmog.pso.topologies;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import pl.edu.pw.mini.zmog.pso.particles.Particle;

/**
 *
 * @author azych
 */
public class LocalNeighbourhood implements Neighbourhood {

    public static int range=2;

    @Getter
    private static LocalNeighbourhood instance = new LocalNeighbourhood();

    @Override
    public List<Particle> getNeighbours(Particle p) {
        List<Particle> swarm = p.getSwarm().getParticles();
        int indx = swarm.indexOf(p);

        List<Particle> neighbours = new ArrayList<>();
        for (int i = 1; i <= range; i++) {
            Particle p1 = swarm.get((indx - i+swarm.size()) % swarm.size());
            if (neighbours.contains(p1))
                neighbours.add(p1);
            Particle p2 = swarm.get((indx + i) % swarm.size());
            if (neighbours.contains(p2))
                neighbours.add(p1);
            neighbours.add(p2);
        }

        return neighbours;
    }

    @Override
    public Particle getBestInNeighbourhood(Particle p) {
        List<Particle> neighbours = getNeighbours(p);

        Particle bestParticle = p;
        double bestResult = p.bestValue;
        for (int i = 0; i < neighbours.size(); i++)
            if (neighbours.get(i).bestValue < bestResult) {
                bestResult = neighbours.get(i).bestValue;
                bestParticle = neighbours.get(i);
            }

        return bestParticle;
    }
}

package pl.edu.pw.mini.zmog.pso.swarms;

import java.util.*;
import lombok.RequiredArgsConstructor;
import org.apache.commons.math3.util.Pair;
import pl.edu.pw.mini.zmog.pso.IQualityFunction;
import pl.edu.pw.mini.zmog.pso.ParticleSwarmOptimization;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.particles.*;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.LocalNeighbourhood;
import pl.edu.pw.mini.zmog.pso.velocity.VelocityUpdateRule;

/**
 *
 *
 * @since 2017-12-02, 14:49:12
 * @author Mateusz Uliński
 */
@RequiredArgsConstructor
public class SwarmManager {

    private final ParticleSwarmOptimization pso;

    public Swarm createSwarm(int size, List<VelocityUpdateRule> rules, double[] velocityProbabilities) {
        IQualityFunction f = pso.getFunction();
        Swarm swarm = new Swarm(pso);
        for (VelocityUpdateRule rule : rules) {
            swarm.getVelocityMap().put(rule, new ArrayList<>());
            swarm.getSuccessMap().put(rule, new ArrayList<>());
        }
        List<Particle> particles = swarm.getParticles();
        for (int i = 0; i < size; i++) {
            final Random generator = pso.getRandomGenerator();
            Pair<double[], Double> init = f.createInitialSolution(generator);
//            double[] velocity = f.createInitialVelocity(generator);
            //System.out.println(Arrays.toString(velocity));

            //StandardPSO
//            Particle p = new StandardParticle(init.getKey(), init.getValue(), velocity, swarm);
            //FullyInformedPSO
//            Particle p = new FullyInformedParticle(init.getKey(), init.getValue(), velocity, swarm);
            //ChargedPSO
//            Particle p = new ChargedParticle(init.getKey(), init.getValue(), velocity, swarm, generator.nextInt(2) == 0 ? ChargedParticle.maxCharge : 0);
            //UnifiedPSO
//            Particle p = new UnifiedParticle(init.getKey(), init.getValue(), velocity, swarm);
//            UnifiedPSO
//            CompositeParticle p = new CompositeParticle(OrthogonalVelocityUpdate.getInstance(), init.getKey(), init.getValue(), velocity, swarm);
//            p.setNeighbourhood(FullNeighbourhood.getInstance());
            //Probabilistic
            Particle p = createParticle(init, null, swarm, rules, velocityProbabilities);

            particles.add(p);
        }
        for(Particle p:particles ) {
            p.initVelocity();
        }
        
        return swarm;
    }

    private Particle createParticle(Pair<double[], Double> init, double[] velocity, Swarm swarm, List<VelocityUpdateRule> rules, double[] velocityProbabilities) {
        ProbabilisticParticle p = new ProbabilisticParticle(init.getKey(), init.getValue(), velocity, swarm);
            p.setNeighbourhoodTopologies(Arrays.asList(FullNeighbourhood.getInstance(), LocalNeighbourhood.getInstance()),
                                         RandomHelper.normalizedCummulative(2));
            if (velocityProbabilities == null)
                p.setVelocityRules(rules, RandomHelper.normalizedCummulative(rules.size()));
            else
                p.setVelocityRules(rules, velocityProbabilities);

        return p;
    }
}

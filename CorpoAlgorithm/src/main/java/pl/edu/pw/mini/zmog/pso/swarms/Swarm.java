package pl.edu.pw.mini.zmog.pso.swarms;

import java.util.*;
import java.util.stream.DoubleStream;
import lombok.Data;
import lombok.Getter;
import pl.edu.pw.mini.zmog.pso.ParticleSwarmOptimization;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.particles.CompositeParticle;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.particles.ProbabilisticParticle;
import pl.edu.pw.mini.zmog.pso.velocity.VelocityUpdateRule;

/**
 *
 *
 * @since 2017-12-02, 14:05:24
 * @author Mateusz Uliński
 */
@Data
public class Swarm {

    private ParticleSwarmOptimization pso;
    private Particle best;
    private Particle bestAllTime;
    private List<Particle> lastBest = new ArrayList<>();
    private List<Particle> particles = new ArrayList<>();
    private List<Double> successRates = new ArrayList<>();
    private Map<VelocityUpdateRule, List<Double>> velocityMap = new HashMap<>();
    private Map<VelocityUpdateRule, List<Double>> successMap = new HashMap<>();
    private Integer bestValueLastChanged = 0;

    public Swarm(ParticleSwarmOptimization pso) {
        this.pso = pso;
    }

    public void turn() {

        if (best != null) {
            lastBest.add(best);
        } else {
            best = particles.get(0);
            for (Particle p : particles) {
                if (p.bestValue < best.bestValue) {
                    best = p;
                }
            }
        }
        double success = 0;
        for (Particle p : particles) {
            p.update();
            //data for adaptation
            if (p.success > 0) {
                success++;
            }
            if (!(p instanceof ProbabilisticParticle)) {
                continue;
            }
            VelocityUpdateRule rule = ((CompositeParticle) p).getVelocityUpdateRule();
            List<Double> velocities = velocityMap.get(rule);
            double sum = DoubleStream.of(p.velocity).map(x -> x * x).sum();
            velocities.add(sum);

            successMap.get(rule).add(p.success);
        }
        successRates.add(success / (double) particles.size());

        bestValueLastChanged++;
        for (Particle p : particles) {
            if (p.bestValue < best.bestValue) {
                best = p;
                bestValueLastChanged = 0;
            }
        }

        if (bestAllTime == null) {
            bestAllTime = best;
        }
        if (best.bestValue < bestAllTime.bestValue) {
            bestAllTime = best;
        }
    }

    public void adjust(int indexToReduce, int indexToIncrease) {
        for (Particle p : particles) {
            if (p instanceof ProbabilisticParticle) {
                ((ProbabilisticParticle) p).adjust(indexToReduce, indexToIncrease);
            }
        }
    }

    //aktualizacja prawdopodobienstw czastki na podstawie slownika: indeks reguly i osiagniety sredni sukces reguly
    public void adjust(Map<Integer, Double> adjustMap) {

        Collections.shuffle(particles, RandomHelper.getRandom()); //mieszamy kolejnoscia czastek, aby narzucac regule roznym czastkom
        //pierszym n czastkom narzucamy regule aktualizacji predkosci, gdzie n to liczba regul
        int k = 0;
        for (Map.Entry<Integer, Double> e : adjustMap.entrySet()) {
            ((ProbabilisticParticle) particles.get(k)).velocityProbabilities = new double[adjustMap.size()];
            ((ProbabilisticParticle) particles.get(k)).velocityProbabilities[e.getKey()] = 1.0;
            k++;
        }

        //pozostale czastki dostaja rozklad prawdopodobienst zgodny z sukcesami
        for (int i = k; i < particles.size(); i++) {
            if (particles.get(i) instanceof ProbabilisticParticle) {
                ((ProbabilisticParticle) particles.get(i)).adjust(adjustMap);
            }
        }
    }
}

package pl.edu.pw.mini.zmog.pso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.DoubleStream;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.edu.pw.mini.zmog.pso.particles.CompositeParticle;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.particles.ProbabilisticParticle;
import pl.edu.pw.mini.zmog.pso.swarms.Swarm;
import pl.edu.pw.mini.zmog.pso.swarms.SwarmManager;
import pl.edu.pw.mini.zmog.pso.velocity.ChargedVelocityUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.DEVelocityUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.FullyInformedUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.OrthogonalVelocityUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.StandardVelocityUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.UnifiedVelocityUpdate;
import pl.edu.pw.mini.zmog.pso.velocity.VelocityUpdateRule;

/**
 *
 *
 * @since 2017-12-02, 13:32:48
 * @author Mateusz Uliński
 */
@Getter
@RequiredArgsConstructor
public class ParticleSwarmOptimization {

    private final IQualityFunction function;
    private final long budget;
    private final Random randomGenerator;
    private SwarmManager swarmManager;

    private List<Swarm> swarms = new ArrayList<>();
    private List<VelocityUpdateRule> rules = Arrays.asList(
            ChargedVelocityUpdate.getInstance(),
            StandardVelocityUpdate.getInstance(),
            FullyInformedUpdate.getInstance(),
            UnifiedVelocityUpdate.getInstance(),
            DEVelocityUpdate.getInstance());

    public final static int TURNS_PER_SWARM = 1;
    public final static int SWARM_SIZE = 30;
    private double oldBest = Double.MAX_VALUE;
    private boolean withAdaptation;

    public static double[] velocityProbability;

    public void init() {
        init(RandomHelper.uniform(0, 1, rules.size()), true);
    }

    public void init(String rule) {
        swarmManager = new SwarmManager(this);
        for (VelocityUpdateRule r : rules) {
            if (r.getClass().getSimpleName().toLowerCase().contains(rule)) {
                System.out.println("inferring " + r.getClass().getSimpleName());
                swarms.add(swarmManager.createSwarm(SWARM_SIZE, Collections.singletonList(r), new double[]{0}));
                return;
            }
        }
    }

    public void init(double[] velocityProbabilities) {
        init(velocityProbabilities, true);
    }

    public void init(double[] velocityProbabilities, boolean withAdaptation) {
        this.withAdaptation = withAdaptation;
        swarmManager = new SwarmManager(this);
        swarms.add(swarmManager.createSwarm(SWARM_SIZE, rules, velocityProbabilities));
//        swarms.add(swarmManager.createSwarm(SWARM_SIZE));
//        swarms.add(swarmManager.createSwarm(SWARM_SIZE));
//        RandomHelper.setRandom(randomGenerator);
    }

    public static String test;

    public void optimize(String logFile) {

        PrintWriter writer = null;
        if (logFile != null) {
            try {
                writer = new PrintWriter(new FileOutputStream(new File(logFile), true));
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ParticleSwarmOptimization.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        for (int j = 0; j * SWARM_SIZE * TURNS_PER_SWARM < budget; j++) {
            swarms.stream()
                    .parallel()
                    .forEach(swarm -> {
                        for (int i = 0; i < TURNS_PER_SWARM; i++) {
                            swarm.turn();
                        }
                        if (RandomHelper.getRandom().nextDouble() < 0.05) {
                            Particle best = swarm.getBest();
                            best.VNS(0.5, SWARM_SIZE * TURNS_PER_SWARM);
                        }
                    });

            if (withAdaptation) {
                outer:
                for (Swarm swarm : swarms) {
//------------- adaptacja na podstawie jakości sukcesów (ruletka z prawdopodobieństwami proporcjonalnymi do sukcesów)
                    Map<VelocityUpdateRule, List<Double>> successMap = swarm.getSuccessMap();
                    Map<Integer, Double> successMapAveraged = new HashMap<>();
                    for (Entry<VelocityUpdateRule, List<Double>> e : successMap.entrySet()) {
                        if (e.getValue().size() > 0) {
                            successMapAveraged.put(rules.indexOf(e.getKey()), e.getValue().stream().mapToDouble(x -> x).average().getAsDouble());
                        } else {
                            successMapAveraged.put(rules.indexOf(e.getKey()), 0.0);
                        }
                        e.setValue(new ArrayList<Double>());
                    }
                    swarm.adjust(successMapAveraged);

                    if (writer != null) {
                        writer.print("success;" + test + ";" + j + ";" + function.getName() + ";");
                        for (int i = 0; i < rules.size(); i++) {
                            writer.print(successMapAveraged.get(i) + ";");
                        }
                        writer.println();
                    }

                    if (writer != null) {
                        writer.print("prob;" + test + ";" + j + ";" + function.getName() + ";");
                        for (int i = 0; i < rules.size(); i++) {
                            writer.print(((ProbabilisticParticle) swarm.getParticles().get(swarm.getParticles().size() - 1)).velocityProbabilities[i] + ";");
                        }
                        writer.println();
                    }
                }
            }

            if (writer != null) {
                writer.print("it;" + test + ";" + j + ";" + function.getName() + ";" + swarms.get(0).getBestAllTime().bestValue + ";");
                int[] rulesCounter = new int[rules.size()];
                for (Particle p : swarms.get(0).getParticles()) {
                    rulesCounter[rules.indexOf(((CompositeParticle) p).getVelocityUpdateRule())]++;
                }
                for (int i = 0; i < rulesCounter.length; i++) {
                    writer.print(rulesCounter[i] + ";");
                }
                writer.println();
            }

            boolean stop = false;
            for (Swarm swarm : swarms) {
                stop &= swarm.getBestValueLastChanged() > 200 * function.getDim();
            }
            if (stop) break;
        }

        for (Swarm swarm : swarms) {
            Particle bestAllTime = swarm.getBestAllTime();
            bestAllTime.VNS(3.5, 1000);
            //System.out.println("vns " + bestAllTime.bestValue);
        }
        if (writer != null) {
            writer.close();
        }
    }

    //testy przeprowadzone w dniu 2018-01-07
    public static void Test001() {
        PrintWriter writer = null;
        try {
            IQualityFunction[] functions = new IQualityFunction[]{
                new IQualityFunction(X -> DoubleStream.of(X).map(x -> x * x).sum(), 2, -1000, 1000, "kwadratowa2D"),
                new IQualityFunction(X -> DoubleStream.of(X).map(x -> x * x).sum(), 10, -1000, 1000, "kwadratowa10D"),
                new IQualityFunction(X -> 10 * 2 + DoubleStream.of(X).map(x -> x * x - 10 * Math.cos(2 * Math.PI * x)).sum(), 2, -100, 100, "Rastrigin2D"),
                new IQualityFunction(X -> 10 * 4 + DoubleStream.of(X).map(x -> x * x - 10 * Math.cos(2 * Math.PI * x)).sum(), 4, -100, 100, "Rastrigin4D"),
                new IQualityFunction(X -> 10 * 6 + DoubleStream.of(X).map(x -> x * x - 10 * Math.cos(2 * Math.PI * x)).sum(), 6, -100, 100, "Rastrigin6D"),
                new IQualityFunction(X -> 10 * 8 + DoubleStream.of(X).map(x -> x * x - 10 * Math.cos(2 * Math.PI * x)).sum(), 8, -100, 100, "Rastrigin8D"),
                new IQualityFunction(X -> 10 * 10 + DoubleStream.of(X).map(x -> x * x - 10 * Math.cos(2 * Math.PI * x)).sum(), 10, -100, 100, "Rastrigin10D")
            };

            String[] testNames = new String[]{"Standard", "Charged", "FullyInformed", "Unified", "DE", "All"};
            double[][] velocityProb = new double[][]{ //{Charged, Standard, FullyInformed, Unified, DE
                //new double[]{0, 1, 0, 0, 0},
                //new double[]{1, 0, 0, 0, 0},
                //new double[]{0, 0, 1, 0, 0},
                new double[]{0, 0, 0, 0, 1}, //                new double[]{0, 0, 0, 0, 1},
            //new double[]{0.2, 0.4, 0.6, 0.8, 1.0}
            };

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
            writer = new PrintWriter("..//Wyniki//test" + dateFormat.format(new Date()) + ".csv", "UTF-8");
            String logFile = "..//Wyniki//log" + dateFormat.format(new Date()) + ".csv";
            int testNumber = 20;
            for (int k = 0; k < testNumber; k++) {
                int fj = -1;
                for (IQualityFunction f : functions) {
                    fj++;
                    for (int i = 0; i < velocityProb.length; i++) {
                        test = testNames[i];
                        ParticleSwarmOptimization pso = new ParticleSwarmOptimization(f, 50, RandomHelper.getRandom());
                        velocityProbability = velocityProb[i];
                        pso.init(velocityProb[i].clone());
                        if (i < 5) {
                            pso.optimize(logFile);
                        } else {
                            pso.optimize(logFile);
                        }
                        writer.println(testNames[i] + ";" + functions[fj].getName() + ";" + pso.getSwarms().get(0).getBestAllTime().bestValue);
                        writer.flush();
                    }
                }
            }
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(ParticleSwarmOptimization.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
    }

    public static void main(String[] args) {
        //      Test001();
        //      return;

        int dim = 5;
//        funkcja kwadratowa 2D
        ParticleSwarmOptimization pso = new ParticleSwarmOptimization(new IQualityFunction(X -> DoubleStream.of(X).map(x -> x * x).sum(), dim, -1000, 1000),
                50, RandomHelper.getRandom());
////        funkcja Rastrigina 3D

//        ParticleSwarmOptimization pso = new ParticleSwarmOptimization(new IQualityFunction(X -> 10 * dim + DoubleStream.of(X).map(x -> x * x - 10 * Math.cos(2 * Math.PI * x)).sum(), dim, -100, 100),
//                                                                      50, RandomHelper.getRandom());
        pso.init("orth");
        pso.optimize(null);
    }

}

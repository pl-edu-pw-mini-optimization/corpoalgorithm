package pl.edu.pw.mini.zmog.pso.memory;

import java.util.*;
import org.apache.commons.math3.util.Pair;

/**
 *
 *
 * @since 2017-12-02, 13:35:14
 * @author Mateusz Uliński
 */
public interface MemoryManager {

    public default void save(double[] x, double[] v) {

    }

    public default List<Pair<double[], double[]>> getMemory() {
        return Collections.EMPTY_LIST;
    }
}

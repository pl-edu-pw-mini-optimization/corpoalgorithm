package pl.edu.pw.mini.zmog.pso.particles;

import pl.edu.pw.mini.zmog.pso.swarms.Swarm;
import pl.edu.pw.mini.zmog.pso.velocity.StandardVelocityUpdate;

/**
 *
 *
 * @since 2017-12-02, 19:19:00
 * @author Mateusz Uliński
 */
public class StandardParticle extends CompositeParticle {

    public StandardParticle(double[] x, double currentValue, double[] velocity, Swarm swarm) {
        super(new StandardVelocityUpdate(), x, currentValue, velocity, swarm);

    }

}

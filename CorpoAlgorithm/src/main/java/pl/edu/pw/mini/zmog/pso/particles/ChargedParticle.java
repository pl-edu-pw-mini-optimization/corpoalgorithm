/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pw.mini.zmog.pso.particles;

import lombok.Setter;
import pl.edu.pw.mini.zmog.pso.swarms.Swarm;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;
import pl.edu.pw.mini.zmog.pso.velocity.ChargedVelocityUpdate;

/**
 *
 * @author azych
 */
public class ChargedParticle extends CompositeParticle {
    
    public static int maxCharge = 32;
    
    public ChargedParticle(double[] x, double currentValue, double[] velocity, Swarm swarm, double charge) {
        super(new ChargedVelocityUpdate(), x, currentValue, velocity, swarm);
        
        this.charge = charge;
        this.setNeighbourhood(new FullNeighbourhood());
    }
}

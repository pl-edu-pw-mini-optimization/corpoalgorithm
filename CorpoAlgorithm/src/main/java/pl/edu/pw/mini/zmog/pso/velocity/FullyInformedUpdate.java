package pl.edu.pw.mini.zmog.pso.velocity;

import java.util.*;
import lombok.Getter;
import org.apache.commons.math3.util.MathArrays;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.LocalNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;

/**
 *
 *
 * @since 2017-12-04, 11:32:36
 * @author Mateusz Uliński
 */
public class FullyInformedUpdate implements VelocityUpdateRule {
    private static double w = 0.9;
    private static double c = 4.5;
    @Getter
    private static FullyInformedUpdate instance = new FullyInformedUpdate();

    @Override
    public double[] calculate(Particle p, Neighbourhood neighbourhood, MemoryManager memoryManager) {
        List<Particle> neighbours = neighbourhood.getNeighbours(p);
        double[] phi = RandomHelper.uniform(0, c / neighbours.size(), neighbours.size());

        double[] x = p.x;
        double[] pm = new double[x.length];
        double sumWeights = 0;
        for (int i = 0; i < neighbours.size(); i++) {
            Particle neighbour = neighbours.get(i);
            double weight = relevance(p) * phi[i];
            double[] scaled = MathArrays.scale(weight, neighbour.bestX);
            pm = MathArrays.ebeAdd(pm, scaled);
            sumWeights += weight;
        }
        MathArrays.scaleInPlace(1 / sumWeights, pm);
        double[] d = MathArrays.ebeSubtract(pm, x);

        double[] velocity = p.velocity;
        velocity = MathArrays.ebeAdd(velocity, d);
        MathArrays.scaleInPlace(w, velocity);
        return velocity;
    }

    private double relevance(Particle p) {
        return 1 / (1+Math.exp(-p.bestValue));
    }

    @Override
    public Neighbourhood getStandardTopology() {
        return LocalNeighbourhood.getInstance();
    }
}

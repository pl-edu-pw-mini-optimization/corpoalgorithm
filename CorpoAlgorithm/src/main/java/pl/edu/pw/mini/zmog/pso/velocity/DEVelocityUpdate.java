/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.pw.mini.zmog.pso.velocity;

import java.util.List;
import lombok.Getter;
import org.apache.commons.math3.util.MathArrays;
import pl.edu.pw.mini.zmog.pso.RandomHelper;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;

/**
 *
 * @author azych
 */
public class DEVelocityUpdate implements VelocityUpdateRule {

    private static double crossProb = 0.5;
    private static double varF = 1.4;

    @Getter
    private static DEVelocityUpdate instance = new DEVelocityUpdate();

    @Override
    public double[] calculate(Particle p, Neighbourhood neighbourhood, MemoryManager memoryManager) {
        double[] x = p.x;
        double[] testVector = p.bestX;
        Particle bestNeighbour = neighbourhood.getBestInNeighbourhood(p);
        List<Particle> swarm = neighbourhood.getSwarm(p);
        Particle firstRandomNeighbour = swarm.get(RandomHelper.getRandom().nextInt(swarm.size()));
        Particle secondRandomNeighbour = swarm.get(RandomHelper.getRandom().nextInt(swarm.size()));
        double[] diffMutationVector = MathArrays.ebeSubtract(
                firstRandomNeighbour.bestX,
                secondRandomNeighbour.bestX);
        diffMutationVector = MathArrays.scale(
                varF * RandomHelper.getRandom().nextDouble(),
                diffMutationVector);
        testVector = MathArrays.ebeAdd(testVector, diffMutationVector);
        for (int i = 0; i < testVector.length; i++) {
            if (RandomHelper.getRandom().nextDouble() < crossProb) {
                testVector[i] =  bestNeighbour.bestX[i];
            }
        }
        return MathArrays.ebeSubtract(testVector, x);
    }

    @Override
    public Neighbourhood getStandardTopology() {
        return FullNeighbourhood.getInstance();
    }
}

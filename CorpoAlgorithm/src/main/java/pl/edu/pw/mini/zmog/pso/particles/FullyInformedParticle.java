package pl.edu.pw.mini.zmog.pso.particles;

import pl.edu.pw.mini.zmog.pso.swarms.Swarm;
import pl.edu.pw.mini.zmog.pso.topologies.FullNeighbourhood;
import pl.edu.pw.mini.zmog.pso.velocity.FullyInformedUpdate;

/**
 *
 *
 * @since 2017-12-02, 18:28:26
 * @author Mateusz Uliński
 */
public class FullyInformedParticle extends CompositeParticle {
    public FullyInformedParticle(double[] x, double currentValue, double[] velocity, Swarm swarm) {
        super(new FullyInformedUpdate(), x, currentValue, velocity, swarm);
        this.setNeighbourhood(new FullNeighbourhood());
    }

}

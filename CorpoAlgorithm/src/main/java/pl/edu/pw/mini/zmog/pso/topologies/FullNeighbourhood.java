package pl.edu.pw.mini.zmog.pso.topologies;

import java.util.*;
import lombok.Getter;
import pl.edu.pw.mini.zmog.pso.particles.Particle;

/**
 *
 *
 * @since 2017-12-02, 13:34:41
 * @author Mateusz Uliński
 */
public class FullNeighbourhood implements Neighbourhood {

    @Getter
    private static FullNeighbourhood instance = new FullNeighbourhood();

    @Override
    public List<Particle> getNeighbours(Particle p) {
        return getSwarm(p);
    }

    @Override
    public Particle getBestInNeighbourhood(Particle p) {
        return getBestInSwarm(p);
    }

}

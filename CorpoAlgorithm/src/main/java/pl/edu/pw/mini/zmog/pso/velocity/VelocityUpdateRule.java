package pl.edu.pw.mini.zmog.pso.velocity;

import java.util.*;
import pl.edu.pw.mini.zmog.pso.memory.MemoryManager;
import pl.edu.pw.mini.zmog.pso.particles.Particle;
import pl.edu.pw.mini.zmog.pso.topologies.Neighbourhood;

/**
 *
 *
 * @since 2017-12-02, 13:34:58
 * @author Mateusz Uliński
 */
public interface VelocityUpdateRule {
    public double[] calculate(Particle p, Neighbourhood neighbourhood, MemoryManager memoryManager);
    public Neighbourhood getStandardTopology();
}

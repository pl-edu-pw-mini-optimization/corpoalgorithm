[01] Clerc, Maurice. "Standard particle swarm optimisation." (2012)

[02] Zhan, Zhi-Hui, et al. "Orthogonal learning particle swarm optimization." IEEE transactions on evolutionary computation 15.6 (2011): 832-847.

[03] Mendes, Rui, James Kennedy, and José Neves. "The fully informed particle swarm: simpler, maybe better." IEEE transactions on evolutionary computation 8.3 (2004): 204-210.

[04] Blackwell, Tim. "Particle swarm optimization in dynamic environments." Evolutionary computation in dynamic and uncertain environments. Springer Berlin Heidelberg, 2007. 29-49.

[05] Parsopoulos, Konstantinos E., and Michael N. Vrahatis. "Unified particle swarm optimization for solving constrained engineering optimization problems." International Conference on Natural Computation. Springer, Berlin, Heidelberg, 2005.

[06] Kennedy, James. "Particle swarm optimization." Encyclopedia of machine learning. Springer US, 2011. 760-766.

[07] Cleghorn, Christopher W., and Andries P. Engelbrecht. "Particle swarm stability: a theoretical extension using the non-stagnate distribution assumption." Swarm Intelligence (2017): 1-22.

[08] Dwa artykułu z konferencji o adaptacji i analizie algorytmów rojowych

[09] Artykuł o uogólnionym PSO i symulacji społecznej
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob.grouproles;

import java.util.Arrays;
import java.util.Set;
import javabbob.Employee;
import javabbob.SamplingEmployee;
import org.apache.commons.math3.ml.clustering.CentroidCluster;

/**
 *
 * @author T540p
 */
public class Coordinator extends SamplingEmployee {

    public Coordinator(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    @Override
    protected double getInterSpeaking() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getInterListening() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getOuterSpeaking() {
        return Employee.LOW_PROBABILITY;
    }

    @Override
    protected double getOuterListening() {
        return Employee.LOW_PROBABILITY;
    }

    @Override
    protected double getOwnHistoryFactor() {
        return 0.0;
    }

    @Override
    protected double getNeighborBestFactor() {
        return 0.2;
    }

    @Override
    protected double getNeighborBestFactorMin() {
        return 0.9;
    }

    @Override
    public void aggregateNeighbours(Set<Integer> neighbours, double[][] best, double[] bestValues, double[][] current, double[] values
    ) {
        gatherSamples(neighbours, current, values);
        if (clusterIterator == null || !clusterIterator.hasNext()) {
            performRandomClustering(neighbours);
            clusterIterator = clusterCenters.iterator();
        }
        CentroidCluster<VectorWithValue> chosen = clusterIterator.next();
        attractionPoint = Arrays.copyOf(chosen.getCenter().getPoint(), chosen.getCenter().getPoint().length);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob.grouproles;

import java.util.Arrays;
import java.util.Set;
import javabbob.Employee;
import javabbob.ParticleSwarmOptimization;
import javabbob.SamplingEmployee;
import org.apache.commons.math3.ml.clustering.CentroidCluster;

/**
 *
 * @author T540p
 */
public class MonitorEvaluator extends SamplingEmployee {

    public MonitorEvaluator(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    @Override
    protected double getInterSpeaking() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getInterListening() {
        return Employee.TYPICAL_PROBABILITY;
    }

    @Override
    protected double getOuterSpeaking() {
        return Employee.ZERO_PROBABILITY;
    }

    @Override
    protected double getOuterListening() {
        return Employee.ZERO_PROBABILITY;
    }

    @Override
    protected double getOwnHistoryFactor() {
        return 0.0;
    }

    @Override
    protected double getNeighborBestFactor() {
        return 0.2;
    }

    @Override
    protected double getNeighborBestFactorMin() {
        return 0.9;
    }

    @Override
    public void aggregateNeighbours(Set<Integer> neighbours, double[][] best, double[] bestValues, double[][] current, double[] values) {
        gatherSamples(neighbours, current, values);
        if (clusterIterator == null || !clusterIterator.hasNext()) {
            performRandomClustering(neighbours);
            clusterIterator = clusterCenters.iterator();
            for (int sampleId = 0; sampleId < values.length; sampleId++) {
                if (maxValue < values[sampleId]) {
                    maxValue = values[sampleId];
                } else if (minValue > values[sampleId]) {
                    minValue = values[sampleId];
                }
            }
        }
        int[] clusterSizes = new int[clusterCenters.size()];
        int samplesCount = samples.size();
        double[] averageClusterValues = new double[clusterCenters.size()];
        int clusterIdx = 0;
        while (clusterIterator.hasNext()) {
            CentroidCluster<VectorWithValue> currentCluster = clusterIterator.next();
            clusterSizes[clusterIdx] = currentCluster.getPoints().size();
            averageClusterValues[clusterIdx] = 0.0;
            for (VectorWithValue sample : currentCluster.getPoints()) {
                averageClusterValues[clusterIdx] += sample.value / clusterSizes[clusterIdx];
            }
            ++clusterIdx;
        }

        double maxActionValue = 0.0;
        int bestIdx = ParticleSwarmOptimization.rand.nextInt(clusterSizes.length);
        for (clusterIdx = 0; clusterIdx < clusterSizes.length; clusterIdx++) {
            double actionUCB1Value = (maxValue - averageClusterValues[clusterIdx] + 1.0) / (maxValue - minValue + 1.0) + 0.5 * Math.sqrt(Math.log(samplesCount) / clusterSizes[clusterIdx]);
            if (actionUCB1Value > maxActionValue) {
                maxActionValue = actionUCB1Value;
                bestIdx = clusterIdx;
            }
        }

        //The center of the cluster with the best value / smallest size balance
        attractionPoint = Arrays.copyOf(clusterCenters.get(bestIdx).getCenter().getPoint(), clusterCenters.get(bestIdx).getCenter().getPoint().length);

    }
    protected double maxValue = Double.NEGATIVE_INFINITY;
    protected double minValue = Double.POSITIVE_INFINITY;

}

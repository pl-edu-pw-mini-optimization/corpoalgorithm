/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import static javabbob.ExampleExperiment.evalsSinceImprovement;
import javabbob.grouproles.CompleterFinisher;
import javabbob.grouproles.Coordinator;
import javabbob.grouproles.Implementer;
import javabbob.grouproles.MonitorEvaluator;
import javabbob.grouproles.Plant;
import javabbob.grouproles.ResourceInvestigator;
import javabbob.grouproles.Shaper;
import javabbob.grouproles.Specialist;
import javabbob.grouproles.Teamworker;

/**
 *
 * @author T540p
 */
public class ParticleSwarmOptimization {

    private void initializeVelocity(int n, int dim, double[][] v, double[][] x, int teamSize) {
        for (int j = 0; j < n; ++j) {
//            if (j % teamSize == 0)
//                rand = new Random(2);
            int p1 = rand.nextInt(n);
            int p2 = rand.nextInt(n);
            for (int i = 0; i < dim; i++) {
                v[j][i] = (x[p1][i] - x[p2][i]) / 2;
            }
        }
    }

    private void initializePosition(int n, int dim, double[][] x, double[] bestValues, int teamSize) {
        for (int j = 0; j < n; ++j) {
//            if (j % teamSize == 0)
//                rand = new Random(1);
            for (int i = 0; i < dim; i++) {
                x[j][i] = 10. * rand.nextDouble() - 5.;
            }
            bestValues[j] = Double.POSITIVE_INFINITY;
        }
    }

    private double performVNS(int dim, double step, double[] bestOfTheBest, JNIfgeneric fgeneric, double bestValue) {
        double[] direction = new double[dim];
        double[] newDirection = new double[dim];
        double[] testPoint = new double[dim];
        double testValue;
        int missedTries = 2 * dim;
        for (int iterations = 0; iterations < dim * 1000; ++iterations) {
            for (int i = 0; i < dim; ++i) {
                newDirection[i] = rand.nextGaussian() * step + direction[i] / 2;
                testPoint[i] = bestOfTheBest[i] + newDirection[i];
            }
            testValue = fgeneric.evaluate(testPoint);
            if (testValue <= bestValue) {
                for (int i = 0; i < dim; ++i) {
                    bestOfTheBest[i] = testPoint[i];
                    direction[i] = newDirection[i];
                    missedTries = 2 * dim;
                }
                bestValue = testValue;
            } else {
                for (int i = 0; i < dim; ++i) {
                    testPoint[i] = bestOfTheBest[i] - newDirection[i];
                }
                testValue = fgeneric.evaluate(testPoint);
                ++iterations;
                if (testValue <= bestValue) {
                    for (int i = 0; i < dim; ++i) {
                        bestOfTheBest[i] = testPoint[i];
                        direction[i] = -newDirection[i];
                        missedTries = 2 * dim;
                    }
                    bestValue = testValue;
                } else {
                    if (--missedTries <= 0) {
                        missedTries = 2 * dim;
                        step /= 1.2;
                    }
                }
            }
        }
        return bestValue;
    }

    private void updateVelocity(int n, int dim, double[][] v, double[][] best, double[][] x) {

        employees
                .stream()
                .parallel()
                .forEach((employee) -> {
            if (employee.getId() < psoN) {
                Implementer(dim, v, employee, best, x);
            } else {
                DEParticle(dim, employee.getId(), n, v, best, x);
            }
        });

    }

    private void DEParticle(int dim, int j, int n1, double[][] v, double[][] best, double[][] x) {
        for (int i = 0; i < dim; i++) {
            int[] deSpecimen = chooseDeSpecimenIdx(j, n1);
            v[j][i] = rand.nextBoolean() ? (best[deSpecimen[2]][i] + deCrossOverProb * (best[deSpecimen[0]][i] - best[deSpecimen[1]][i])) : best[j][i]
                    - x[j][i];
        }
    }

    private void Implementer(int dim, double[][] v, Employee employee, double[][] best, double[][] x) {
        //System.err.println(employeeId);
        int employeeId = employee.getId();
        for (int i = 0; i < dim; i++) {
            v[employeeId][i] = omega * v[employeeId][i]
                    + employee.getOwnHistoryFactor() * rand.nextDouble() * (best[employeeId][i] - x[employeeId][i])
                    + (employee.getNeighborBestFactor() * rand.nextDouble() + employee.getNeighborBestFactorMin()) * (employee.getAttractionPoint()[i] - x[employeeId][i]);
        }
    }

    private int[] chooseDeSpecimenIdx(int j, int n1) {
        int[] deSpecimen = new int[]{j, j, j};
        while (deSpecimen[0] == j) {
            deSpecimen[0] = rand.nextInt(n1);
        }
        while (deSpecimen[1] == j || deSpecimen[1] == deSpecimen[0]) {
            deSpecimen[1] = rand.nextInt(n1);
        }
        while (deSpecimen[2] == j || deSpecimen[2] == deSpecimen[1] || deSpecimen[1] == deSpecimen[0]) {
            deSpecimen[2] = rand.nextInt(n1);
        }
        return deSpecimen;
    }

    private void updateLocation(int n, int dim, double[][] x, double[][] v) {
        for (int j = 0; j < n; ++j) {
            for (int i = 0; i < dim; i++) {
                x[j][i] = x[j][i] + v[j][i];
            }
        }
    }

    private double updateBest(int n, double[] f, double[] bestValues, int dim, double[][] best, double[][] x, double bestValue, double[] bestOfTheBest) {
        Set[] neighbors = new Set[n];
        employees
                .stream()
                .parallel()
                .forEach((employee) -> {
            int employeeId = employee.getId();
            neighbors[employeeId] = new HashSet<>();
            for (int neighborIdx = 0; neighborIdx < n; neighborIdx++) {
                if (rand.nextDouble() < employee.probabilityOfGettingInformationFromCoworker(employees.get(neighborIdx))) {
                    neighbors[employeeId].add(neighborIdx);
                }
            }
            if (f[employeeId] <= bestValues[employeeId]) {
                for (int i = 0; i < dim; ++i) {
                    best[employeeId][i] = x[employeeId][i];
                }
                bestValues[employeeId] = f[employeeId];
            }
        });

        int bestResult = -1;
        for (int i = 0; i < bestValues.length; i++) {
            ++evalsSinceImprovement;
            if (bestValues[i] <= bestValue) {
                evalsSinceImprovement = 0;
                bestValue = bestValues[i];
                bestResult = i;
            }
        }
        for (int i = 0; i < dim; ++i) {
            bestOfTheBest[i] = best[bestResult][i];
        }

        employees
                .stream()
                .parallel()
                .forEach((employee) -> {
            employee.aggregateNeighbours(neighbors[employee.getId()], best, bestValues, x, f);
        });
        return bestValue;
    }

    private void computeFunction(int n, double[] f, JNIfgeneric fgeneric, double[][] x) {
        /* evaluate X on the objective function */
        for (int j = 0; j < n; ++j) {
            f[j] = fgeneric.evaluate(x[j]);
        }
    }

    int n;
    /**
     * DE switched off
     */
    int psoN;
    double deCrossOverProb = 0.2;
    //double c1 = 1.4;
    //double c2 = 1.4;
    double omega = 0.64;

    List<Employee> employees;
    public static Random rand;

    public double[] optimize(JNIfgeneric fgeneric, int dim, double maxfunevals, Random random, int[][] teams) {
        int teamCount = teams.length;
        int teamSize = (teams[0].length - 1);
        psoN = n = teamCount * teamSize;
        double[][] x = new double[n][dim];
        double[][] best = new double[n][dim];
        double[][] v = new double[n][dim];

        employees = new ArrayList<>(teamSize * teamCount);
        for (int teamNo = 0; teamNo < teamCount; teamNo++) {
            for (int employeeNo = 0; employeeNo < teamSize; employeeNo++) {
                switch (teams[teamNo][employeeNo]) {
                    case 0: employees.add(new CompleterFinisher(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 1: employees.add(new Coordinator(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 2: employees.add(new Implementer(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 3: employees.add(new MonitorEvaluator(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 4: employees.add(new Plant(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 5: employees.add(new ResourceInvestigator(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 6: employees.add(new Shaper(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 7: employees.add(new Specialist(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                    case 8: employees.add(new Teamworker(
                            teamNo * teamSize + employeeNo,
                            teamNo,
                            employeeNo == teams[teamNo][teamSize]));
                    break;
                }
            }
        }

        rand = random;

        /* Obtain the target function value, which only use is termination */
        double ftarget = fgeneric.getFtarget();
        double[] f = new double[n];
        double[] bestValues = new double[n];
        double bestValue = Double.MAX_VALUE;
        double[] bestOfTheBest = new double[dim];

//        rand = new Random(1);

        long seed = System.currentTimeMillis();
        rand = new Random(seed);

        initializePosition(n, dim, x, bestValues,teamSize);
        initializeVelocity(n, dim, v, x,teamSize);
        computeFunction(n, f, fgeneric, x);
        bestValue = updateBest(n, f, bestValues, dim, best, x, bestValue, bestOfTheBest);

        if (maxfunevals > 1e9 * dim) {
            maxfunevals = 1e9 * dim;
        }

        for (double iter = 0.; iter < maxfunevals; iter += n) {
            updateLocation(n, dim, x, v);
            computeFunction(n, f, fgeneric, x);
            bestValue = updateBest(n, f, bestValues, dim, best, x, bestValue, bestOfTheBest);

            updateVelocity(n, dim, v, best, x);
            if (bestValue < ftarget) {
                break;
            }

            if (evalsSinceImprovement > dim * 100) {
                break;
            }

        }
        
        performVNS(dim, 0.5, bestOfTheBest, fgeneric, bestValue);
        double[] teamResults = new double[teamCount];
        for (int teamNo = 0; teamNo < teamCount; teamNo++) {
            teamResults[teamNo] = Double.POSITIVE_INFINITY;
            for (int teamMember = 0; teamMember < teamSize; teamMember++) {
                if (bestValues[teamNo * teamSize + teamMember] < teamResults[teamNo])
                    teamResults[teamNo] = bestValues[teamNo * teamSize + teamMember];
            }
        }
        
        return teamResults;
    }

}

package javabbob;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.MathIllegalStateException;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.MultivariateSummaryStatistics;

/**
 * Wrapper class running an entire BBOB experiment. It illustrates the
 * benchmarking of MY_OPTIMIZER on the noise-free testbed or the noisy testbed
 * (change the ifun loop in this case as given below). This class reimplements
 * the content of exampleexperiment.c from the original C version of the BBOB
 * code.
 */
public class ExampleExperiment {

    static int[][] population;

    /**
     * Example optimiser. In the following, the pure random search optimization
     * method is implemented as an example. Please include/insert any code as
     * suitable.<p>
     * This optimiser takes as argument an instance of JNIfgeneric which have
     * all the information on the problem to solve. Only the methods
     * getFtarget() and evaluate(x) of the class JNIfgeneric are used.<p>
     * This method also takes as argument an instance of Random since one might
     * want to set the seed of the random search.<p>
     * The optimiser generates random vectors evaluated on fgeneric until the
     * number of function evaluations is greater than maxfunevals or a function
     * value smaller than the target given by fgeneric.getFtarget() is attained.
     * The parameter maxfunevals to avoid problem when comparing it to
     * 1000000000*dim where dim is the dimension of the problem.
     *
     * @param fgeneric an instance JNIfgeneric object
     * @param dim an integer giving the dimension of the problem
     * @param maxfunevals the maximum number of function evaluations
     * @param rand an instance of Random
     */
    public static double[] optimizeByPso(JNIfgeneric fgeneric, int dim, double maxfunevals, Random rand, int[][] population) {
        return new ParticleSwarmOptimization().optimize(fgeneric, dim, maxfunevals, rand, population);

    }

    static int evalsSinceImprovement = 0;

    /**
     * Main method for running the whole BBOB experiment. Executing this method
     * runs the experiment. The first command-line input argument is
     * interpreted: if given, it denotes the data directory to write in (in
     * which case it overrides the one assigned in the preamble of the method).
     */
    public static void main(String[] args) throws IOException {

        if (args.length < 4) {
            System.out.println("Usage:");
            System.out.println("CorpoAlgorithm.jar [inner iterations] [name] [EA iterations] [random] [[initial function] [step]] [[singleTeamRole]]");
            System.exit(0);
        }

        //For decimal point to be a ','
        Locale.setDefault(new Locale("pl", "PL"));
        /* run variables for the function/dimension/instances loops */
        //final int dim[] = {2, 3, 5, 10, 20, 40};
        final int dim[] = {5, 20};
        final int instances[] = {1, 2, 3, 4, 5, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50};
        int idx_dim, ifun, idx_instances, independent_restarts;
        double maxfunevals;
        double evalsPerDim = 5e1;
        String outputPath;

        JNIfgeneric fgeneric = new JNIfgeneric();
        /* The line above loads the library cjavabbob at the core of
         * JNIfgeneric. It will throw runtime errors if the library is not
         * found.
         */

        /**
         * ************************************************
         * BBOB Mandatory initialization *
         * ***********************************************
         */
        JNIfgeneric.Params params = new JNIfgeneric.Params();
        /* Modify the following parameters, choosing a different setting
         * for each new experiment */
        params.algName = "PSO_DE_LM_VNS";
        params.comments = "PSO hybrydized with DE particles and LM particle and VNS at the end";
        int eaIterations = 0;
        boolean averageTeams = false;
        if (args.length > 0) {
            evalsPerDim = Double.parseDouble(args[0]);
        }

        if (args.length > 1) {
            params.algName = args[1];
        }

        if (args.length > 2) {
            eaIterations = Integer.parseInt(args[2]);
        }

        if (args.length > 3) {
            averageTeams = Boolean.getBoolean(args[3]);
        }

        int initFID = 1;
        int stepFID = 1;
        if (args.length > 5) {
            initFID = Integer.parseInt(args[4]);
            stepFID = Integer.parseInt(args[5]);
        }

        int singleTeamRole = -1;
        if (args.length > 6) {
            singleTeamRole = Integer.parseInt(args[6]);
        }
        outputPath = params.algName + evalsPerDim;

        /* Creates the folders for storing the experimental data. */
        if (JNIfgeneric.makeBBOBdirs(outputPath, args.length < 5)) {
//            System.out.println("BBOB data directories at " + outputPath
//                    + " created.");
        } else {
//            System.out.println("Error! BBOB data directories at " + outputPath
//                    + " was NOT created, stopping.");
            return;
        };


        /* External initialization of MY_OPTIMIZER */
        long seed = System.currentTimeMillis();
        Random rand = new Random(seed);
        //System.out.println("MY_OPTIMIZER seed: " + seed);

        /* record starting time (also useful as random number generation seed) */
        long t0 = System.currentTimeMillis();

        /* To make the noise deterministic, uncomment the following block. */
        /* int noiseseed = 30; // or (int)t0
         * fgeneric.setNoiseSeed(noiseseed);
         * System.out.println("seed for the noise set to: "+noiseseed); */

        /* now the main loop */
        System.out.println("Evaluations\tIteration\tFunction\tValue");
        for (idx_dim = 0; idx_dim < dim.length; idx_dim++) {
            /* Function indices are from 1 to 24 (noiseless) or from 101 to 130 (noisy) */
            /* for (ifun = 101; ifun <= 130; ifun++) { // Noisy testbed */
            for (ifun = initFID; ifun <= 24; ifun += stepFID) { //Noiseless testbed
                int teams = 4;
                int teamSize = 10;
                double[][] results = new double[teams][instances.length];
//for learning                
//                initializeTeams(teams, teamSize, averageTeams, ifun, singleTeamRole);
//                for (int i = 0; i < teams; ++i) {
//                    System.out.print(population[i][population[i][teamSize]]);
//                    System.out.print(" ");
//                }
//                System.out.println();
                double[] retEval = null;
                for (int eaIteration = 0; eaIteration < eaIterations; ++eaIteration) {
                    for (idx_instances = 0; idx_instances < instances.length; idx_instances++) {

                        /* Initialize the objective function in fgeneric. */
                        fgeneric.initBBOB(ifun, instances[idx_instances],
                                dim[idx_dim], outputPath, params);
                        /* Call to the optimizer with fgeneric as input */
                        maxfunevals = evalsPerDim * teams * teamSize * dim[idx_dim]; /* PUT APPROPRIATE MAX. FEVALS */
                        /* 5. * dim is fine to just check everything */

                        double[] teamResults = new double[teams];
                        independent_restarts = -1;
                        while (fgeneric.getEvaluations() < maxfunevals) {
                            //for running
                            initializeTeams(teams, teamSize, averageTeams, ifun, singleTeamRole);
                            independent_restarts++;
                            teamResults = optimizeByPso(fgeneric, dim[idx_dim],
                                    maxfunevals - fgeneric.getEvaluations(), rand, population);
                            if (fgeneric.getBest() < fgeneric.getFtarget()) {
                                break;
                            }
                        }

//                    System.out.printf("  f%d in %d-D, instance %d: FEs=%.0f with %d restarts,", ifun, dim[idx_dim],
//                            instances[idx_instances], fgeneric.getEvaluations(), independent_restarts);
//                    System.out.printf(" fbest-ftarget=%.4e, elapsed time [h]: %.2f\n", fgeneric.getBest() - fgeneric.getFtarget(),
//                            (double) (System.currentTimeMillis() - t0) / 3600000.);
                        for (int team = 0; team < teams; team++) {
                            results[team][idx_instances] = teamResults[team] - fgeneric.getFtarget();
                        }
                        /* call the BBOB closing function to wrap things up neatly */
                        fgeneric.exitBBOB();
                    }
                    System.out.printf("%.0f\t%d\t%d\t", evalsPerDim * teams * teamSize * dim[idx_dim], eaIteration, ifun);
                    retEval = evaluationAndSelection(teams, results, rand, teamSize);
                    crossOverTeams(teamSize, rand);
                    mutateTeams(teamSize, rand);
//                    System.out.println(population[0][population[0][teamSize]]);
//                    System.out.println(Arrays.toString(population[2]));
                }

                int bestTeam = -1;
                double bestValue = Double.POSITIVE_INFINITY;
                for (int i = 0; i < teams / 2; ++i) {
                    System.err.print(population[i][population[i][teamSize]]);
                    System.err.print(" ");
                    if (retEval[i] < bestValue) {
                        bestValue = retEval[i];
                        bestTeam = i;
                    }
                }
                for (int team = 0; team < teams; team++) {
                    System.err.print("\t");
                    for (int i = 0; i < population[team].length - 1; ++i) {
                        System.err.print(population[team][i]);
                        System.err.print(" ");
                    }
                }
                System.err.println();

//                System.out.println("\ndate and time: " + (new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")).format(
//                        (Calendar.getInstance()).getTime()));
            }
//            System.out.println("---- dimension " + dim[idx_dim] + "-D done ----");
        }
        //system(paste0("python python\\bbob_pproc\\rungeneric.py ",name));
        //system(paste0("pdflatex -jobname ",name," templateCECBBOBarticle.tex >log.log"))
        //system(paste0("pdflatex -jobname ",name," templateCECBBOBarticle.tex >log.log"))

        Runtime.getRuntime().exec("python ../BBOB/python/bbob_pproc/rungeneric.py " + outputPath);
    }

    protected static double[] evaluationAndSelection(int teams, double[][] results, Random rand, int teamSize) throws MathIllegalStateException, MathIllegalArgumentException {
        double min = Double.POSITIVE_INFINITY;
        double[] evaluation = new double[teams];
        double[] retEval = new double[teams];
        for (int team = 0; team < teams; team++) {
            DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics(results[team]);
            //evaluation[team] = descriptiveStatistics.getPercentile(0.95);
            evaluation[team] = descriptiveStatistics.getMean();
            if (evaluation[team] < min) {
                min = evaluation[team];
            }
        }
        System.out.printf("%.8f\n", min);

        List<Integer> winningTeams = new ArrayList<>();
        for (int team = 0; team < teams / 2; team++) {
            int first = team;
            int second = team + teams / 2;
            if (evaluation[first] < evaluation[second]) {
                winningTeams.add(first);
            } else {
                winningTeams.add(second);
            }
        }
        int[][] tempPopulation = new int[teams][teamSize + 1];
        int place = 0;
        for (int teamId : winningTeams) {
            retEval[place] = evaluation[teamId];
            retEval[place + teams / 2] = evaluation[teamId];
            tempPopulation[place] = Arrays.copyOf(population[teamId], teamSize + 1);
            tempPopulation[place + teams / 2] = Arrays.copyOf(population[teamId], teamSize + 1);
            ++place;
        }
        population = tempPopulation;
        return retEval;
    }

//    protected static void initializeTeams(int teams, int teamSize, boolean randomTeams) {
//        Random random = new Random();
//        population = new int[teams][teamSize + 1];
//        for (int teamNo = 0; teamNo < teams; teamNo++) {
//            population[teamNo][teamSize] = randomTeams ? random.nextInt(teamSize) : (teamNo % teamSize);
//            for (int employeeNo = 0; employeeNo < teamSize; employeeNo++) {
//                population[teamNo][employeeNo] = randomTeams ? random.nextInt(9) : (employeeNo % 9);
//            }
//        }
//    }

    static int[] averagePM = new int[]{0, 0, 0, 0, 2, 6, 7, 8};
    static int[] averageTeam = new int[]{0, 1, 2, 3, 4, 0, 5, 6, 7, 8};

    static int[][] functionPMs = new int[][]{
        {0, 0, 1, 2, 3, 7, 7},
        {0, 0, 0, 2, 2, 6, 6, 7},
        {0, 0, 2, 5, 6, 6, 7, 8},
        {0, 0, 0, 2, 4, 6, 6},
        {0, 2, 5, 6, 6, 6, 6, 7},
        {0, 0, 0, 0, 0, 0, 2, 6},
        {0, 0, 0, 0, 2, 6, 7, 8},
        {0, 0, 0, 0, 0, 0, 3},
        {0, 0, 0, 0, 3, 4, 5, 7},
        {0, 0, 0, 0, 0, 1, 7},
        {0, 0, 0, 0, 7, 8, 8},
        {0, 0, 0, 0, 1, 2, 2, 6},
        {0, 0, 0, 0, 1, 2, 4, 8},
        {0, 0, 0, 0, 4, 5, 6},
        {0, 0, 0, 0, 2, 6, 7, 8},
        {0, 0, 0, 0, 0, 0, 2, 7},
        {0, 0, 0, 0, 3, 4, 7, 7},
        {0, 0, 0, 0, 1, 2, 5, 8},
        {2, 2, 3, 8, 8, 8, 8},
        {0, 0, 0, 2, 2, 4, 6, 6},
        {0, 0, 0, 0, 2, 7},
        {0, 0, 1, 2, 6, 7},
        {0, 0, 0, 3, 5, 5, 8},
        {0, 0, 0, 0, 2, 6, 8}
    };
    static int[][] functionTeams = new int[][]{
        {0, 0, 1, 2, 3, 5, 6, 7, 7},
        {0, 0, 0, 2, 2, 6, 6, 7},
        {0, 0, 2, 5, 6, 6, 7, 8},
        {0, 0, 0, 2, 4, 6, 6},
        {0, 2, 5, 6, 6, 6, 6, 7},
        {0, 0, 0, 0, 0, 2, 6},
        {0, 0, 0, 2, 6, 7, 8},
        {0, 0, 0, 0, 0, 3},
        {0, 0, 0, 0, 3, 4, 5, 6, 7},
        {0, 0, 0, 0, 1, 2, 7},
        {0, 0, 0, 1, 7, 8, 8},
        {0, 0, 0, 1, 2, 2, 6},
        {0, 0, 0, 0, 1, 2, 4, 8},
        {0, 0, 0, 0, 4, 5, 6, 8},
        {0, 0, 0, 0, 2, 6, 7, 8},
        {0, 0, 0, 0, 0, 2, 7},
        {0, 0, 0, 0, 3, 4, 7, 7},
        {0, 0, 0, 0, 1, 2, 5, 8},
        {0, 2, 2, 3, 5, 8, 8, 8, 8},
        {0, 0, 0, 2, 2, 4, 6, 6, 8},
        {0, 0, 0, 1, 2, 3, 4, 7, 8},
        {0, 0, 1, 2, 6, 7},
        {0, 0, 0, 3, 5, 5, 8},
        {0, 0, 0, 0, 2, 6, 8}
    };

    protected static void initializeTeams(int teams, int teamSize, boolean useAverageTeam, int functionId, int singleRole) {
        Random random = new Random();
        population = new int[teams][teamSize + 1];
        for (int teamNo = 0; teamNo < teams; teamNo++) {
            population[teamNo][teamSize] = 0;
            if (singleRole > -1) {
                population[teamNo][0] = singleRole;
            } else if (useAverageTeam) {
                population[teamNo][0] = averagePM[random.nextInt(averagePM.length)];
            } else {
                population[teamNo][0] = functionPMs[functionId - 1][random.nextInt(functionPMs[functionId - 1].length)];
            }
            for (int employeeNo = 1; employeeNo < teamSize; employeeNo++) {
                if (singleRole > - 1) {
                    population[teamNo][employeeNo] = singleRole;
                } else if (useAverageTeam) {
                    population[teamNo][employeeNo] = averageTeam[random.nextInt(averageTeam.length)];
                } else {
                    population[teamNo][employeeNo] = functionTeams[functionId - 1][random.nextInt(functionTeams[functionId - 1].length)];
                }
            }
        }
    }

    protected static void mutateTeams(int teamSize, Random rand) {
        for (int teamNo = population.length / 2; teamNo < population.length; teamNo++) {
            double chancePM = rand.nextDouble();
            if (chancePM < pmMutationChangeModifier / teamSize) {
                population[teamNo][teamSize] = rand.nextInt(teamSize);
            }
            for (int employeeNo = 0; employeeNo < teamSize; employeeNo++) {
                double chance = rand.nextDouble();
                if (chance < employeeMutationModifier / teamSize && population[teamNo][teamSize] != employeeNo) {
                    population[teamNo][employeeNo] = rand.nextInt(9);
                } else if (chance < pmMutationModifier / teamSize && population[teamNo][teamSize] == employeeNo) {
                    population[teamNo][employeeNo] = rand.nextInt(9);
                }

            }
        }
    }

    protected static double employeeMutationModifier = 1.0;
    protected static double pmMutationChangeModifier = 0.01;
    protected static double pmMutationModifier = 1.0;

    protected static void crossOverTeams(int teamSize, Random rand) {
        for (int teamNo = population.length / 2; teamNo < population.length; teamNo++) {
            double chanceCross = rand.nextDouble();
            if (chanceCross < 0.5) {
                int secondTeam = rand.nextInt(population.length / 2);
                population[teamNo][population[teamNo][teamSize]] = population[secondTeam][population[secondTeam][teamSize]];
            }
        }
    }
}

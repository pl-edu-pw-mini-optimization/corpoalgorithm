/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabbob;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;

/**
 *
 * @author T540p
 */
public abstract class SamplingEmployee extends Employee {

    public class VectorWithValue implements Clusterable {

        public double[] x;
        public double value;

        public VectorWithValue(double[] x, double value) {
            this.x = Arrays.copyOf(x, x.length);
            this.value = value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof VectorWithValue) {
                return Arrays.equals(this.x, ((VectorWithValue) obj).x);
            }
            return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public int hashCode() {
            int hash = (int) Math.round(value);
            return hash;
        }

        @Override
        public double[] getPoint() {
            return x;
        }

    }

    protected HashSet<VectorWithValue> samples = new HashSet<>();
    protected List<CentroidCluster<VectorWithValue>> clusterCenters;
    protected Iterator<CentroidCluster<VectorWithValue>> clusterIterator = null;

    public SamplingEmployee(int employeeId, int teamId, boolean leader) {
        super(employeeId, teamId, leader);
    }

    protected void performRandomClustering(Set<Integer> neighbours) throws MathIllegalArgumentException, ConvergenceException {
        do {
            KMeansPlusPlusClusterer<VectorWithValue> clusterer
                    = new KMeansPlusPlusClusterer<>(ParticleSwarmOptimization.rand.nextInt(neighbours.size() / 2 + 1));
            try {
                clusterCenters = clusterer.cluster(samples);
            } catch (NumberIsTooSmallException ex) {

            }
        } while (clusterCenters == null);
    }

    protected void gatherSamples(Set<Integer> neighbours, double[][] current, double[] values) {
        for (int neighbor : neighbours) {
            samples.add(new VectorWithValue(current[neighbor], values[neighbor]));
        }
    }

}

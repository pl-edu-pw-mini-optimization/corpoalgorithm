# README #

Aplikacja jest w Javie (z wykorzystaniem mavena).

Jest lokalna DLLka z benchmarkami zbudowana z repozytorium centralnego coco, która powinna w pierwszym kroku skopiować się do lokalnego repozytorium maven'a aby następnie móc być używana i zarządzana przez mavena.

Runtime parameters:

* `java -jar CorpoAlgorithm-1.0.jar ALGORITHM DIM FUN BUDGET`

Examples:

* java -jar CorpoAlgorithm-1.0.jar de 5 all 10000
* java -jar CorpoAlgorithm-1.0.jar all 5,20 f2 10000
* java -jar CorpoAlgorithm-1.0.jar all all f1,f13 100000


##Szkic architektury##

![CorpoAlgorithmOrganization.png](https://bitbucket.org/repo/kM74zde/images/3618983109-CorpoAlgorithmOrganization.png)

